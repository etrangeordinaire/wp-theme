var max_id = null;
jQuery(document).ready(function($) {
	if(!$('.tweets').length) return;
	$('.tweets-more').click(function(e) {
		$(this).addClass('is-loading');
		loadtweets();
	})

	loadtweets();
});

function loadtweets() {
	$.get(site_url+'/api/tweets', { q: hashtag, max_id: max_id }, function(data) {
		$('.tweets-list').append(data);
		max_id = $('.tweet:last-child').attr('data-tweetId');
		$('.tweets-more').removeClass('is-loading');
	});
}