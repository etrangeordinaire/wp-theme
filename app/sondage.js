
// toggle description
$('.proposition-header').on('click', function() {
	var parent = $(this).closest('.proposition');
	parent.toggleClass('is-active');
	parent.find('.proposition-body').slideToggle();
});


// vote
$('[data-vote]').on('click', function(e) {
	e.preventDefault();
	e.stopPropagation();

	var element = $(this);

	var vote = element.attr('data-vote');
	var itemId = element.attr('data-voteId');
	if(!itemId || !vote || element.hasClass('has-voted')) return false;

	var list = element.closest('.proposition-ctas');
	var hasVotedElement = list.find('.has-voted');
	if(hasVotedElement.length) var hasVotedCount = parseInt(hasVotedElement.find('.proposition-cta-count').html());
	var elementCount = parseInt(element.find('.proposition-cta-count').html());

	list.addClass('disabled');


	var url = site_url+'/api/proposition/vote/'+itemId+'/'+vote
	$.ajax({
		type: 'post',
		url: url,
		success: function() {
			element.closest('.proposition-ctas').removeClass('disabled');
			if(hasVotedElement) hasVotedElement.removeClass('has-voted').find('.proposition-cta-count').html(hasVotedCount-1);
			element.addClass('has-voted').find('.proposition-cta-count').html(elementCount+1);
		}
	})
});