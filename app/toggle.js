jQuery(document).ready(function($) {
	$('[data-toggleClass]').click(function(e) {
		e.preventDefault();
		var targetId = $(this).attr('data-targetId');
		var toggleClass = $(this).attr('data-toggleClass');
		if(!targetId || !toggleClass) return;

		$('#'+targetId).toggleClass(toggleClass);
	})
})