// comment lightbox before
$('[data-lightbox="commentForm"]').on('click', function() {
	var itemId = $(this).attr('data-itemId');
	$('.commentForm-form').attr('data-itemId', itemId);
});

// comment form
$(document).on('submit', '.commentForm-form', function(e) {
	e.preventDefault();
	e.stopPropagation();

	var $form = $(this);
	var itemId = $form.attr('data-itemId');
	if(!itemId) return false;

	var $message = $form.find('.form-message');
	$message.removeClass('is-success is-error');
	$form.find('.form-field').removeClass('form-error');
	$form.find('.form-error-msg').html('');

	var url = site_url+'/api/comment/'+itemId
	$form.find('.form-button').addClass('disabled');

	$.post(url, $form.serialize())
		.done(function(data) {
			$form.find('.form-fieldset').hide();
			$message.addClass('is-success').html("Votre message a bien été enregistré. Il sera publié après validation d'un administrateur.");
		})
		.fail(function(data) {
			$message.addClass('is-error').html("Veuillez corriger les erreurs ci-dessous.");
			$.each(data.responseJSON, function(key, value) {
				var $input = $form.find('[name="'+key+'"]');
				$input.closest('.form-field').addClass('form-error').find('.form-error-msg').html(value);
			});
		})
		.always(function() {
			$form.find('.form-button').removeClass('disabled');
		})
		;
})