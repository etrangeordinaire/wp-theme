var Media = require('./media');

$('.slick').each(function(key, element) {
	var $element = $(element);

	// init
	$element.slick({
		autoplay: $element.attr('data-autoplay') == 'false' ? false : true,
		autoplaySpeed: 5000,
		infinite: true,
		dots: 'mobile' != Media.media && $element.attr('data-dots') != 'false',
		arrows: 'mobile' != Media.media
	});
	
	// unset autoplay
	$element.on('click', function() {
		$element.slick('slickSetOption', 'autoplay', false);
	});

	// slide finished
	var slides = $element.find('.slick-slide:not(.slick-cloned)');
	setTimeout(function() {
		$(slides.get(0)).addClass('slide-finished');
	}, 700);
	$element.on('afterChange', function(event, slick, currentSlide) {
		slides.removeClass('slide-finished');
		$(slides.get(currentSlide)).addClass('slide-finished');
	});
	
});