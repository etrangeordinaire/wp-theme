var styles = [
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape",
    "stylers": [
      {
        "color": "#c6b9bb"
      },
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#833855"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
];

var markerLatlng = {lat: 43.834621, lng: 4.349514};
var centerLatlng = {lat: 43.838, lng: 4.349514};
var mapOptions = {
	center: centerLatlng,
	zoom: 15,
	mapTypeControl: false,
	scaleControl: false,
	zoomControl: true,
	streetViewControl: false,
	clickableIcons: false,
	disableDoubleClickZoom: true,
	scrollwheel: false,
	styles: styles
};
var map, icon;

var initMap = function initMap() {
  if(!$('.map-container').length) return;

  icon = {
      url: template_dir+'/assets/images/geomarker.png',
      anchor: new google.maps.Point(75,145),
      size: new google.maps.Size(150,145),
      zIndex: 10
    };

  map = new google.maps.Map($('.map-container').get(0), mapOptions);

  var marker = new google.maps.Marker({
    position: markerLatlng,
    map: map,
    title: 'étrangeOrdinaire',
    icon: icon
  });
}

module.exports = initMap;