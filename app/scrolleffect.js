var debug = true;


var controller = new ScrollMagic.Controller({
	addIndicators: debug
});

// var scene = new ScrollMagic.Scene({
// 		triggerElement: "#trigger1",
// 		duration: $("#trigger1").outerHeight() + 1000,
// 		offset: -500
// 	})
// 	.setClassToggle("#trigger1", "active") // add class toggle
// 	.addTo(controller);

var actionZone = 300;
$('[data-scrolleffect=list]').each(function() {
	var $elements = $(this).children();
	var sectionHeight = actionZone / $elements.length;

	$elements.each(function(key, value) {
		new ScrollMagic.Scene({
			triggerElement: this,
			duration: sectionHeight,
			offset: -actionZone + (key) * sectionHeight
		}).setClassToggle(this, 'is-active')
		.addTo(controller);
	});
})

$('[data-scrolleffect=bgparallax]').each(function() {
	new ScrollMagic.Scene({
		triggerElement: this,
		duration: this.offsetHeight + 1000,
		offset: -500
	})
	.setTween(this, 0.5, {backgroundPosition: "left -200px"}) // trigger a TweenMax.to tween
	// .setTween(this, 0.5, { backgroundColor: "green" })
	.addTo(controller);
})