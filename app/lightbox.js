var Media = require('./media');

var $lightbox = $('.lightbox');
var $content = $('.lightbox-content');

$('[data-lightbox]').on('click', function(e) {
	e.preventDefault();
	e.stopPropagation();

	$('body').addClass('noscroll');

	// fill content in lightbox
	var content = $('[data-lightboxid='+$(this).attr('data-lightbox')+']').html();
	$content.html(content);
	$lightbox.addClass('visible');
});

// close lightbox
$lightbox.find('.lightbox-overlay, .lightbox-close').on('click', function(e) {
	$content.empty();
	$('body').removeClass('noscroll');
	$lightbox.removeClass('visible');
});