window.$ = jQuery;

require('./jquery.functions');
require('./scrolled');
require('./slider');
require('./menu');
require('./datahref');
require('./scrolleffect');
require('./lightbox');
require('./toggle');
require('./tweets');
require('./sondage');
require('./expression');
require('./commentForm');

var initMap = require('./map');
_.assign(window, {
  initMap: initMap
});

// exclude IE 10
if ( navigator.userAgent.indexOf('MSIE 10.0') !== -1 ) {
	location.href = '/ie10';
}

// loading body
$(document).ready(function() {
	$('body').removeClass('is-loading');
});

$('a').on('click', function(e) {
	var el = e.delegateTarget;

	// internal links
	if (location.host === el.host && '_blank' != el.getAttribute('target') && el.href.indexOf('#') == -1) {
		if (!e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey) {
			e.preventDefault();
			$('body').addClass('is-loading');
			setTimeout(function() {
				location.href = el.href;
			}, 500);
		}
	}
});