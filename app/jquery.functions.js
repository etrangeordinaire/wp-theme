jQuery.fn.extend({
    
    /*
     * get ajax attr
     */

    getAjaxAttr: function () {
        var $ = jQuery;

        var ajax_attr = {};

        $(this).each(function() {
          $.each(this.attributes, function() {
            var patt=/data-ajax-attr-/g;
            if(this.specified && patt.test(this.name) ) {
              ajax_attr[this.name.replace(/data-ajax-attr-/g, '')] = this.value;
            }
          });
        });

        return ajax_attr;
    },


    /*
     * serialize form
     */

    serializeObject: function() {
        var $ = jQuery;

        var o = {};
        var a = $(this).serializeArray();

        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    },


    /*
     * Test if element has specfic parent
     */

    hasParent: function(p) {
        var $ = jQuery;

        // Return truthy/falsey based on presence in parent
        return $(this).parents(p).length > 0 || $(this).is(p);
    }
});