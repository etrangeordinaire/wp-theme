
// toggle comments
// $('.proposition-header').on('click', function() {
// 	var parent = $(this).closest('.proposition');
// 	parent.toggleClass('is-active');
// 	parent.find('.proposition-body').slideToggle();
// });


// like
$('[data-like]').on('click', function(e) {
	e.preventDefault();
	e.stopPropagation();

	var element = $(this);
	var itemId = element.attr('data-like');
	if(!itemId) return false;

	var hasLiked = element.hasClass('has-liked');
	var hasLikeCount = parseInt(element.find('.expression-item-reactions-count').html());

	var url = site_url+'/api/expression/like/'+itemId
	element.addClass('disabled');
	$.ajax({
		type: 'post',
		url: url,
		success: function() {
			element.removeClass('disabled');
			element.toggleClass('has-liked');
			element.find('.expression-item-reactions-count').html(hasLiked ? hasLikeCount-1 : hasLikeCount+1);
		}
	})
});



// comments list
$('.expression-item-comments').each(function() {
	var $comments = $(this).find('.expression-item-comments-item');
	console.log($comments);
	if($comments.lengh < 3) return;

	$comments.each(function(key) {
		if(key>2) $(this).hide();
	});
});

// show all comments
$('.expression-item-comments-more').on('click', function() {
	$(this).closest('.expression-item-comments').find('.expression-item-comments-item').show();
	$(this).remove();
});

