		</main>

		<footer class="footer">
			
			<div class="footer-inner">
				<div class="footer-item">
					<div class="footer-heading">Contact</div>
					<div class="footer-description"><?= get_field('contact', 'option') ?></div>
				</div>
				<div class="footer-item">
					<div class="footer-heading">Réseaux sociaux</div>
					<div class="footer-description"><?= get_field('networks', 'option') ?></div>
				</div>
				<div class="footer-item">
					<div class="footer-heading">Prochaines dates</div>
					<div class="footer-description"><?= get_field('legal', 'option') ?></div>
				</div>
				
				<div class="footer-item">
					<div class="footer-heading">Liens utiles</div>
					<div class="footer-description"><?= get_field('links', 'option') ?></div>
			</div>
		</footer>

		<? get_view('lightbox') ?>

		
		<div id="svg-store"><!-- inject:svg --><!-- endinject --></div>

		<!--[if lte IE 9]>
		<? get_view('ie10'); ?>
		<![endif]-->
		
		<? wp_footer() ?>
		
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/jquery.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/lodash.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/slick.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/tweenmax.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/scrollmagic.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/debug.addIndicators.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/animation.gsap.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/dist/app.js<%= killCache %>"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKSFTHZadCKrGrWOWgpLkHVJq_2iarYY8&callback=initMap"></script>
	</body>
</html>