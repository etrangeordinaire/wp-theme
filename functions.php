<?php

define('AUTOSAVE_INTERVAL', 300 ); // seconds
define('WP_POST_REVISIONS', false );

/*
 * Include php
 */

require "vendor/autoload.php";
require_once(TEMPLATEPATH.'/includes/OpenGraph.php');
require_once(TEMPLATEPATH.'/includes/acf.php');

require_once(TEMPLATEPATH.'/includes/fields.php');

require_once(TEMPLATEPATH.'/includes/tools.php');
require_once(TEMPLATEPATH.'/includes/admin.php');
require_once(TEMPLATEPATH.'/includes/register.php');
require_once(TEMPLATEPATH.'/includes/routing.php');
require_once(TEMPLATEPATH.'/includes/shortcodes.php');
require_once(TEMPLATEPATH.'/includes/twitter.php');

use phpFastCache\CacheManager;

// Fast Cache
CacheManager::setup(array(
    "path" => TEMPLATEPATH.'/cache'
));
?>
