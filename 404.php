<? 
get_header('compiled'); 
get_view('mainHeader');
?>
<div class="error">
	<h1 class="error-heading"><i>Erreur</i> <b>404</b></h1>	
	<p class="error-description">La page demandée est introuvable, veuillez utiliser le menu ci-dessus pour reprendre la navigation.</p>
</div>
<? 
get_view('mainFooter');
get_footer('compiled'); 
?>