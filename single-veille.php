<?
global $post;
get_header('compiled');
?>
<div class="page-inner veille">
	<h1 class="veille-title"><?= the_title() ?></h1>
	<div class="veille-content"><?= apply_filters('the_content', get_field('content')) ?></div>
	<div class="veille-author">Publié par <?= getFullName($post->post_author) ?></div>
</div>
<? get_footer('compiled'); ?>