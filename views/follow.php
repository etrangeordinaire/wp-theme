<div class="follow">
		<div class="follow-inner">
			<div class="follow-icon">
				<? icon('atom') ?>
			</div>
			<h2 class="follow-heading">Suivez-nous</h2>
			<p class="follow-description">
				Les dernières actualitées sur nos projets, nos formations <br>
				et nos événements directement dans votre boite mail !
			</p>

		<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup" class="follow-newsletter">
			<form action="//etrangeordinaire.us13.list-manage.com/subscribe/post?u=062d552b0e8194f812a8c1b7f&amp;id=20541f7976" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form" target="_blank" novalidate>
			    <div id="mc_embed_signup_scroll" class="follow-fieldset">
					<input type="email" value="" name="EMAIL" placeholder="Adresse courriel" class="required email" id="mce-EMAIL">

					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_062d552b0e8194f812a8c1b7f_20541f7976" tabindex="-1" value=""></div>

				    <input type="submit" value="Je m'inscris" name="subscribe" id="mc-embedded-subscribe" class="button">
			    </div>
			</form>
		</div>
		<!--End mc_embed_signup-->


	</div>
</div>
