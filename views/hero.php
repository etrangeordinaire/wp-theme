<?
global $post, $adminLink;
?>
<div class="hero" style="background-image:url(<?= get_field('hero_bgimage')['sizes']['slider'] ?>)">

	<div class="hero-main">
		<h1 class="hero-title"><?= $post->post_title ?></h1>
		
		<? if(get_field('hero_subtitle')): ?>
		<h2 class="hero-subtitle"><?= get_field('hero_subtitle') ?></h2>
		<? endif; ?>
	</div>

</div>