<?
use phpFastCache\CacheManager;
global $veille;

$type = get_field('type', $veille->ID);

if('link' == $type) :
	$InstanceCache = CacheManager::getInstance('files');
	$CachedOG = $InstanceCache->getItem('veille_'.$veille->ID);

	$link = get_field('lien', $veille->ID);
	
	if(is_null($CachedOG->get())) {
		$cache = OpenGraph::fetch($link);
		$CachedOG->set($cache)->expiresAfter(3600);
	    $InstanceCache->save($CachedOG);
	}

	$ogData = $CachedOG->get();

	if($ogData) {
		$ogTitle = $ogData->__get('title');
		$ogImage = $ogData->__get('image');
	}
endif;

$title = $ogTitle ? $ogTitle : $veille->post_title;
$bgImage = $ogImage ? $ogImage : get_field('image', $veille->ID)['sizes']['thumbnail'];
?>
<a class="preview <?= $bgImage ? 'has-bgimage' : ''; ?>" target="_blank" href="<?= get_field('lien', $veille->ID) ?>" 
	style="background-image:url(<?= $bgImage ?>)">
	<div class="preview-header">
		<h3 class="preview-title"><?= $title ?></h3>

		<?
		if(get_field('subtitle', $veille->ID)):
		?>
		<h4 class="preview-subtitle"><?= get_field('subtitle', $veille->ID) ?></h4>
		<?
		endif;
		?>
	</div>
	<span class="preview-more">
		<? if('link' == $type): ?>
		Voir le site <? icon('external') ?>
		<? else: ?>
		En savoir plus
		<? endif; ?>
	</span>
</a>