<?
$mainColor = get_field('mainColor', 'options');
$secondaryColor = get_field('secondaryColor', 'option');
$menuTextColor = get_field('menuTextColor', 'option');
?>
<style type="text/css">
.header {
	background-color: <?= $mainColor ?>;
	border-color: <?= $secondaryColor ?>;
}

.header-logo img {
	width: <?= get_field('logo_width', 'option') ?>px;
	transform: translateY(<?= get_field('logo_top', 'option') ?>px);
}

.header-menu .menu-item {
	color: <?= $menuTextColor ?>;
}

.header-menu .menu-item:hover, 
.header-menu .menu-item.current-menu-item, 
.header-menu .menu-item.current-page-ancestor, 
.single-timeline .header-menu .menu-item.menu-calendrier, 
.tax-sondage .header-menu .menu-item.menu-sondages {
    color: <?= $secondaryColor ?>;
}

.header-menu .menu-item.current-menu-item::after, 
.header-menu .menu-item.current-page-ancestor::after, 
.single-timeline .header-menu .menu-item.menu-calendrier::after, 
.tax-sondage .header-menu .menu-item.menu-sondages::after {
    border-bottom-color: <?= $secondaryColor ?>;
}


.expression-item {
	background-color: <?= $mainColor ?>;
	color: <?= $secondaryColor ?>;
}

.expression-item::before {
    border-top-color: <?= $mainColor ?>;
}

.form-button {
	background-color: <?= $secondaryColor ?>;
}
.lightbox-content {
	background-color: <?= $mainColor ?>;
}
.lightbox-title {
	color: <?= $secondaryColor ?>;
}
</style>