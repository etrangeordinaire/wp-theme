<?
global $contactSubject;
?>
<form class="form ajax" action="<?= site_url() ?>/contact/send" method="POST">
	<div class="form-message"></div>
	<input class="form-input" name="name" placeholder="Nom & Prénom *">
	<input class="form-input" name="email" placeholder="Email *">
	<input class="form-input" name="phone" placeholder="Téléphone">
	<input class="form-input" name="subject" placeholder="Que pouvons-nous faire ? *" value="<?= $contactSubject ?>">
	<textarea class="form-textarea" name="message" placeholder="Parlez nous de votre idée... *"></textarea>
	<button class="form-button">Envoyer</button>
</form>