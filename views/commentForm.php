<div class="commentForm" data-lightboxid="commentForm">
	<h3 class="lightbox-title">Laissez un commentaire...</h3>
	<form class="commentForm-form">

		<div class="form-message"></div>

		<fieldset class="form-fieldset">
			<div class="form-field">
				<div class="form-error-msg"></div>
				<input class="form-input" name="name" type="text" placeholder="Nom à afficher" value="">
			</div>
			<div class="form-field">
				<div class="form-error-msg"></div>
				<input class="form-input" name="email" type="text" placeholder="Adresse courriel" value="">
			</div>
			<div class="form-field">
				<div class="form-error-msg"></div>
				<textarea class="form-textarea" name="comment" placeholder="Votre commentaire..."></textarea>
			</div>
			<button class="form-button">Envoyer</button>
		</fieldset>
	</form>
</div>