<!doctype html>
<!--[if lte IE 9 ]><html class="browser-not-compatible"><![endif]-->
<!--[if (gt IE 9)|!(IE) ]><html><![endif]-->
<head>

	<title><? wp_title( '|', true, 'right' ); ?></title>

    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="Content-Language" content="fr_FR" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

    <link rel="shortcut icon" href="<?= get_stylesheet_directory_uri() ?>/assets/images/favicon-120x120.png">
    <link rel="apple-touch-icon" href="<?= get_stylesheet_directory_uri() ?>/assets/images/favicon-120x120.png">

    <meta name="google-site-verification" content="" />

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link href="<?= get_stylesheet_directory_uri() ?>/dist/app.css?noCache=97ab3430" rel="stylesheet" />
    
    <? get_view('css') ?>

    <script>
        var template_dir = "<?= get_stylesheet_directory_uri() ?>";
        var site_url = "<?= site_url() ?>";
    </script>

    <? 
    global $post, $parent;
    wp_head();
    ?>
</head>

<body <? body_class() ?>>
    
    <!-- <div class="loading">
        <img class="loading-logo" src="<?= get_stylesheet_directory_uri() ?>/assets/images/logo-loader.svg">
    </div> -->

    <header class="header">
        <div class="header-inner">
            <a href='<?= site_url() ?>' class="header-logo">
                <img src="<?= get_field('logo', 'option')['url'] ?>">
            </a>

            <? wp_nav_menu(['theme_location'=>'header', 'container' =>  false, 'menu_class' => 'header-menu']); ?>

            <a class="header-menuToggle">
                <? icon('burger', 'icon-open') ?>
                <? icon('cross', 'icon-close') ?>
            </a>
            
            <?
            /* 
            $userId = get_current_user_id();
            
            if($userId): 
                ?>
                <a class="header-login" href="<?= site_url() ?>/profile/<?= $userId ?>"><?= getShortName($userId) ?></a>
                <?
            else:
                ?>
                <a class="header-login" href="<?= site_url() ?>/login">Login</a>
                <?
            endif;
            */
            ?>
        </div>

    </header>

    <? wp_nav_menu(['theme_location'=>'mobile', 'container' =>  false, 'menu_class' => 'menuMobile']); ?>

    <main>

        <?
        if(get_field('partners', 'option')):
            ?>
            <div class="partners">
                <div class="partners-inner">
                    <?
                    if(get_field('partners_list', 'option')):
                        ?>
                        <div class="partners-list">
                            <?
                            foreach(get_field('partners_list', 'option') as $item):
                                ?>
                                <a class="partners-item" href="<?= $item['url'] ?>" title="<?= $item['title'] ?>">
                                    <img class="partners-visual" src="<?= $item['logo']['url'] ?>" alt="<?= $item['title'] ?>">
                                </a>
                                <?
                            endforeach;
                            ?>
                        </div>
                        <?
                    endif;
                    ?>
                </div>
            </div>
            <?
        endif;
        ?>