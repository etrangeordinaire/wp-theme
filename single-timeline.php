<?
global $post;
get_header('compiled');
?>

<div class="timeline">

	<div class="hero" style="background-image:url(<?= get_field('bgimage')['sizes']['slider'] ?>)">
		<div class="hero-main">
			<h1 class="hero-title"><?= $post->post_title ?></h1>
			<? if(get_field('subtitle')): ?>
			<h2 class="hero-subtitle"><?= get_field('subtitle') ?></h2>
			<? endif; ?>
		</div>
	</div>
	<div class="timeline-info">

		<div class="timeline-presentation">
			<h2 class="timeline-presentation-title">Présentation</h2>
			<div class="timeline-presentation-content"><?= get_field('presentation') ?></div>
		</div>

		
		<div class="timeline-aside">
			

			<div class="timeline-aside-block">
				<h2 class="timeline-aside-title">Date</h2>
				<? showDate($post); ?>
			</div>

			<div class="timeline-aside-block">
				<h2 class="timeline-aside-title">Lieux</h2>
				<div class="timeline-aside-content"><?= get_field('place') ?></div>
			</div>

			<div class="timeline-aside-block">
				<h2 class="timeline-aside-title">Acteurs</h2>
				<div class="timeline-aside-content"><?= get_field('team') ?></div>
			</div>

			<?
			if(get_field('intervenants')):
				?>
				<div class="timeline-aside-block">
					<h2 class="timeline-aside-title">Intervenants</h2>
					<ul class="timeline-intervenants">
						<?
						foreach(get_field('intervenants') as $item):
							?>
							<li class="timeline-intervenant">
								<img class="timeline-intervenant-visual" src="<?= get_field('visual', $item->ID)['sizes']['thumbnail'] ?>">
								<div class="timeline-intervenant-main">
									<h3 class="timeline-intervenant-name"><?= $item->post_title ?></h3>
									<div class="timeline-intervenant-description"><?= get_field('description', $item->ID) ?></div>

									<? if(get_field('url', $item->ID)): ?>
									<a class="timeline-intervenant-link" href="<?= get_field('url', $item->ID) ?>"><?= get_field('url', $item->ID) ?></a>
									<? endif; ?>
								</div>
							</li>
							<?
						endforeach;
						?>
					</ul>
				</div>
				<?
			endif;
			?>

			<?
			if(get_field('livrables')):
				?>
				<div class="timeline-aside-block">
					<h2 class="timeline-aside-title">Livrables</h2>
					<ul class="timeline-livrables">
						<?
						foreach(get_field('livrables') as $item):
							?>
							<li class="timeline-livrable">
								<a href="<?= $item['url'] ?>">&rarr; <?= $item['name'] ?></a>
							</li>
							<?
						endforeach;
						?>
					</ul>
				</div>
				<?
			endif;
			?>

		</div>
	</div>

	<? 
	$veilles = get_field('veilles');
	if($veilles): 
		?>
		<div class="timeline-dossier dossier is-active">
			<div class="dossier-header">
				<h2 class="dossier-title">À lire avant...</h2>
			</div>
			<div class="previews">
				<?
				foreach($veilles as $veille):
					get_view('veillePreview');
				endforeach;
				?>
			</div>
		</div>
		<? 
	endif; 
	?>

	<?
	eovc();
	?>


</div>
<? get_footer('compiled'); ?>