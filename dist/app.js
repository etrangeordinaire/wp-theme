(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// comment lightbox before
$('[data-lightbox="commentForm"]').on('click', function() {
	var itemId = $(this).attr('data-itemId');
	$('.commentForm-form').attr('data-itemId', itemId);
});

// comment form
$(document).on('submit', '.commentForm-form', function(e) {
	e.preventDefault();
	e.stopPropagation();

	var $form = $(this);
	var itemId = $form.attr('data-itemId');
	if(!itemId) return false;

	var $message = $form.find('.form-message');
	$message.removeClass('is-success is-error');
	$form.find('.form-field').removeClass('form-error');
	$form.find('.form-error-msg').html('');

	var url = site_url+'/api/comment/'+itemId
	$form.find('.form-button').addClass('disabled');

	$.post(url, $form.serialize())
		.done(function(data) {
			$form.find('.form-fieldset').hide();
			$message.addClass('is-success').html("Votre message a bien été enregistré. Il sera publié après validation d'un administrateur.");
		})
		.fail(function(data) {
			$message.addClass('is-error').html("Veuillez corriger les erreurs ci-dessous.");
			$.each(data.responseJSON, function(key, value) {
				var $input = $form.find('[name="'+key+'"]');
				$input.closest('.form-field').addClass('form-error').find('.form-error-msg').html(value);
			});
		})
		.always(function() {
			$form.find('.form-button').removeClass('disabled');
		})
		;
})
},{}],2:[function(require,module,exports){
$('[data-href]').on('click', function() {
	location.href = $(this).attr('data-href');
});
},{}],3:[function(require,module,exports){

// toggle comments
// $('.proposition-header').on('click', function() {
// 	var parent = $(this).closest('.proposition');
// 	parent.toggleClass('is-active');
// 	parent.find('.proposition-body').slideToggle();
// });


// like
$('[data-like]').on('click', function(e) {
	e.preventDefault();
	e.stopPropagation();

	var element = $(this);
	var itemId = element.attr('data-like');
	if(!itemId) return false;

	var hasLiked = element.hasClass('has-liked');
	var hasLikeCount = parseInt(element.find('.expression-item-reactions-count').html());

	var url = site_url+'/api/expression/like/'+itemId
	element.addClass('disabled');
	$.ajax({
		type: 'post',
		url: url,
		success: function() {
			element.removeClass('disabled');
			element.toggleClass('has-liked');
			element.find('.expression-item-reactions-count').html(hasLiked ? hasLikeCount-1 : hasLikeCount+1);
		}
	})
});



// comments list
$('.expression-item-comments').each(function() {
	var $comments = $(this).find('.expression-item-comments-item');
	console.log($comments);
	if($comments.lengh < 3) return;

	$comments.each(function(key) {
		if(key>2) $(this).hide();
	});
});

// show all comments
$('.expression-item-comments-more').on('click', function() {
	$(this).closest('.expression-item-comments').find('.expression-item-comments-item').show();
	$(this).remove();
});


},{}],4:[function(require,module,exports){
window.$ = jQuery;

require('./jquery.functions');
require('./scrolled');
require('./slider');
require('./menu');
require('./datahref');
require('./scrolleffect');
require('./lightbox');
require('./toggle');
require('./tweets');
require('./sondage');
require('./expression');
require('./commentForm');

var initMap = require('./map');
_.assign(window, {
  initMap: initMap
});

// exclude IE 10
if ( navigator.userAgent.indexOf('MSIE 10.0') !== -1 ) {
	location.href = '/ie10';
}

// loading body
$(document).ready(function() {
	$('body').removeClass('is-loading');
});

$('a').on('click', function(e) {
	var el = e.delegateTarget;

	// internal links
	if (location.host === el.host && '_blank' != el.getAttribute('target') && el.href.indexOf('#') == -1) {
		if (!e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey) {
			e.preventDefault();
			$('body').addClass('is-loading');
			setTimeout(function() {
				location.href = el.href;
			}, 500);
		}
	}
});
},{"./commentForm":1,"./datahref":2,"./expression":3,"./jquery.functions":5,"./lightbox":6,"./map":7,"./menu":9,"./scrolled":10,"./scrolleffect":11,"./slider":12,"./sondage":13,"./toggle":14,"./tweets":15}],5:[function(require,module,exports){
jQuery.fn.extend({
    
    /*
     * get ajax attr
     */

    getAjaxAttr: function () {
        var $ = jQuery;

        var ajax_attr = {};

        $(this).each(function() {
          $.each(this.attributes, function() {
            var patt=/data-ajax-attr-/g;
            if(this.specified && patt.test(this.name) ) {
              ajax_attr[this.name.replace(/data-ajax-attr-/g, '')] = this.value;
            }
          });
        });

        return ajax_attr;
    },


    /*
     * serialize form
     */

    serializeObject: function() {
        var $ = jQuery;

        var o = {};
        var a = $(this).serializeArray();

        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    },


    /*
     * Test if element has specfic parent
     */

    hasParent: function(p) {
        var $ = jQuery;

        // Return truthy/falsey based on presence in parent
        return $(this).parents(p).length > 0 || $(this).is(p);
    }
});
},{}],6:[function(require,module,exports){
var Media = require('./media');

var $lightbox = $('.lightbox');
var $content = $('.lightbox-content');

$('[data-lightbox]').on('click', function(e) {
	e.preventDefault();
	e.stopPropagation();

	$('body').addClass('noscroll');

	// fill content in lightbox
	var content = $('[data-lightboxid='+$(this).attr('data-lightbox')+']').html();
	$content.html(content);
	$lightbox.addClass('visible');
});

// close lightbox
$lightbox.find('.lightbox-overlay, .lightbox-close').on('click', function(e) {
	$content.empty();
	$('body').removeClass('noscroll');
	$lightbox.removeClass('visible');
});
},{"./media":8}],7:[function(require,module,exports){
var styles = [
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape",
    "stylers": [
      {
        "color": "#c6b9bb"
      },
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#833855"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
];

var markerLatlng = {lat: 43.834621, lng: 4.349514};
var centerLatlng = {lat: 43.838, lng: 4.349514};
var mapOptions = {
	center: centerLatlng,
	zoom: 15,
	mapTypeControl: false,
	scaleControl: false,
	zoomControl: true,
	streetViewControl: false,
	clickableIcons: false,
	disableDoubleClickZoom: true,
	scrollwheel: false,
	styles: styles
};
var map, icon;

var initMap = function initMap() {
  if(!$('.map-container').length) return;

  icon = {
      url: template_dir+'/assets/images/geomarker.png',
      anchor: new google.maps.Point(75,145),
      size: new google.maps.Size(150,145),
      zIndex: 10
    };

  map = new google.maps.Map($('.map-container').get(0), mapOptions);

  var marker = new google.maps.Marker({
    position: markerLatlng,
    map: map,
    title: 'étrangeOrdinaire',
    icon: icon
  });
}

module.exports = initMap;
},{}],8:[function(require,module,exports){
'use strict';

/**
 * Dependencies.
 */
var Events = require('backbone-events-standalone');

// mobile breakpoint
var mqMobile = matchMedia('(max-width: 679px)');
mqMobile.addListener(mediaChange);

// tablet breakpoint
var mqTablet = matchMedia('(min-width: 680px) and (max-width: 1023px)');
mqTablet.addListener(mediaChange);

/**
 * [Media description]
 * @type {Object}
 */
var Media = _.extend({}, Events);

mediaChange(true);

function mediaChange(silent) {
  Media.media = mqMobile.matches
    ? 'mobile' : mqTablet.matches
    ? 'tablet' : 'desktop';

  if (silent)
    Media.trigger('change');
}

/**
 * Exports.
 */

module.exports = Media;

},{"backbone-events-standalone":17}],9:[function(require,module,exports){
var $menuMobile = $('.menuMobile');
var $toggle = $('.header-menuToggle');

$toggle.on('click', function() {
	$menuMobile.toggleClass('is-visible');
	$toggle.toggleClass('is-active');

});
},{}],10:[function(require,module,exports){
var $body = $('body');
$(document).on('scroll', function() {
	var top = $(document).scrollTop();
	if(top > 100) $body.addClass('scrolled');
	else $body.removeClass('scrolled');
})

},{}],11:[function(require,module,exports){
var debug = true;


var controller = new ScrollMagic.Controller({
	addIndicators: debug
});

// var scene = new ScrollMagic.Scene({
// 		triggerElement: "#trigger1",
// 		duration: $("#trigger1").outerHeight() + 1000,
// 		offset: -500
// 	})
// 	.setClassToggle("#trigger1", "active") // add class toggle
// 	.addTo(controller);

var actionZone = 300;
$('[data-scrolleffect=list]').each(function() {
	var $elements = $(this).children();
	var sectionHeight = actionZone / $elements.length;

	$elements.each(function(key, value) {
		new ScrollMagic.Scene({
			triggerElement: this,
			duration: sectionHeight,
			offset: -actionZone + (key) * sectionHeight
		}).setClassToggle(this, 'is-active')
		.addTo(controller);
	});
})

$('[data-scrolleffect=bgparallax]').each(function() {
	new ScrollMagic.Scene({
		triggerElement: this,
		duration: this.offsetHeight + 1000,
		offset: -500
	})
	.setTween(this, 0.5, {backgroundPosition: "left -200px"}) // trigger a TweenMax.to tween
	// .setTween(this, 0.5, { backgroundColor: "green" })
	.addTo(controller);
})
},{}],12:[function(require,module,exports){
var Media = require('./media');

$('.slick').each(function(key, element) {
	var $element = $(element);

	// init
	$element.slick({
		autoplay: $element.attr('data-autoplay') == 'false' ? false : true,
		autoplaySpeed: 5000,
		infinite: true,
		dots: 'mobile' != Media.media && $element.attr('data-dots') != 'false',
		arrows: 'mobile' != Media.media
	});
	
	// unset autoplay
	$element.on('click', function() {
		$element.slick('slickSetOption', 'autoplay', false);
	});

	// slide finished
	var slides = $element.find('.slick-slide:not(.slick-cloned)');
	setTimeout(function() {
		$(slides.get(0)).addClass('slide-finished');
	}, 700);
	$element.on('afterChange', function(event, slick, currentSlide) {
		slides.removeClass('slide-finished');
		$(slides.get(currentSlide)).addClass('slide-finished');
	});
	
});
},{"./media":8}],13:[function(require,module,exports){

// toggle description
$('.proposition-header').on('click', function() {
	var parent = $(this).closest('.proposition');
	parent.toggleClass('is-active');
	parent.find('.proposition-body').slideToggle();
});


// vote
$('[data-vote]').on('click', function(e) {
	e.preventDefault();
	e.stopPropagation();

	var element = $(this);

	var vote = element.attr('data-vote');
	var itemId = element.attr('data-voteId');
	if(!itemId || !vote || element.hasClass('has-voted')) return false;

	var list = element.closest('.proposition-ctas');
	var hasVotedElement = list.find('.has-voted');
	if(hasVotedElement.length) var hasVotedCount = parseInt(hasVotedElement.find('.proposition-cta-count').html());
	var elementCount = parseInt(element.find('.proposition-cta-count').html());

	list.addClass('disabled');


	var url = site_url+'/api/proposition/vote/'+itemId+'/'+vote
	$.ajax({
		type: 'post',
		url: url,
		success: function() {
			element.closest('.proposition-ctas').removeClass('disabled');
			if(hasVotedElement) hasVotedElement.removeClass('has-voted').find('.proposition-cta-count').html(hasVotedCount-1);
			element.addClass('has-voted').find('.proposition-cta-count').html(elementCount+1);
		}
	})
});
},{}],14:[function(require,module,exports){
jQuery(document).ready(function($) {
	$('[data-toggleClass]').click(function(e) {
		e.preventDefault();
		var targetId = $(this).attr('data-targetId');
		var toggleClass = $(this).attr('data-toggleClass');
		if(!targetId || !toggleClass) return;

		$('#'+targetId).toggleClass(toggleClass);
	})
})
},{}],15:[function(require,module,exports){
var max_id = null;
jQuery(document).ready(function($) {
	if(!$('.tweets').length) return;
	$('.tweets-more').click(function(e) {
		$(this).addClass('is-loading');
		loadtweets();
	})

	loadtweets();
});

function loadtweets() {
	$.get(site_url+'/api/tweets', { q: hashtag, max_id: max_id }, function(data) {
		$('.tweets-list').append(data);
		max_id = $('.tweet:last-child').attr('data-tweetId');
		$('.tweets-more').removeClass('is-loading');
	});
}
},{}],16:[function(require,module,exports){
/**
 * Standalone extraction of Backbone.Events, no external dependency required.
 * Degrades nicely when Backone/underscore are already available in the current
 * global context.
 *
 * Note that docs suggest to use underscore's `_.extend()` method to add Events
 * support to some given object. A `mixin()` method has been added to the Events
 * prototype to avoid using underscore for that sole purpose:
 *
 *     var myEventEmitter = BackboneEvents.mixin({});
 *
 * Or for a function constructor:
 *
 *     function MyConstructor(){}
 *     MyConstructor.prototype.foo = function(){}
 *     BackboneEvents.mixin(MyConstructor.prototype);
 *
 * (c) 2009-2013 Jeremy Ashkenas, DocumentCloud Inc.
 * (c) 2013 Nicolas Perriault
 */
/* global exports:true, define, module */
(function() {
  var root = this,
      nativeForEach = Array.prototype.forEach,
      hasOwnProperty = Object.prototype.hasOwnProperty,
      slice = Array.prototype.slice,
      idCounter = 0;

  // Returns a partial implementation matching the minimal API subset required
  // by Backbone.Events
  function miniscore() {
    return {
      keys: Object.keys || function (obj) {
        if (typeof obj !== "object" && typeof obj !== "function" || obj === null) {
          throw new TypeError("keys() called on a non-object");
        }
        var key, keys = [];
        for (key in obj) {
          if (obj.hasOwnProperty(key)) {
            keys[keys.length] = key;
          }
        }
        return keys;
      },

      uniqueId: function(prefix) {
        var id = ++idCounter + '';
        return prefix ? prefix + id : id;
      },

      has: function(obj, key) {
        return hasOwnProperty.call(obj, key);
      },

      each: function(obj, iterator, context) {
        if (obj == null) return;
        if (nativeForEach && obj.forEach === nativeForEach) {
          obj.forEach(iterator, context);
        } else if (obj.length === +obj.length) {
          for (var i = 0, l = obj.length; i < l; i++) {
            iterator.call(context, obj[i], i, obj);
          }
        } else {
          for (var key in obj) {
            if (this.has(obj, key)) {
              iterator.call(context, obj[key], key, obj);
            }
          }
        }
      },

      once: function(func) {
        var ran = false, memo;
        return function() {
          if (ran) return memo;
          ran = true;
          memo = func.apply(this, arguments);
          func = null;
          return memo;
        };
      }
    };
  }

  var _ = miniscore(), Events;

  // Backbone.Events
  // ---------------

  // A module that can be mixed in to *any object* in order to provide it with
  // custom events. You may bind with `on` or remove with `off` callback
  // functions to an event; `trigger`-ing an event fires all callbacks in
  // succession.
  //
  //     var object = {};
  //     _.extend(object, Backbone.Events);
  //     object.on('expand', function(){ alert('expanded'); });
  //     object.trigger('expand');
  //
  Events = {

    // Bind an event to a `callback` function. Passing `"all"` will bind
    // the callback to all events fired.
    on: function(name, callback, context) {
      if (!eventsApi(this, 'on', name, [callback, context]) || !callback) return this;
      this._events || (this._events = {});
      var events = this._events[name] || (this._events[name] = []);
      events.push({callback: callback, context: context, ctx: context || this});
      return this;
    },

    // Bind an event to only be triggered a single time. After the first time
    // the callback is invoked, it will be removed.
    once: function(name, callback, context) {
      if (!eventsApi(this, 'once', name, [callback, context]) || !callback) return this;
      var self = this;
      var once = _.once(function() {
        self.off(name, once);
        callback.apply(this, arguments);
      });
      once._callback = callback;
      return this.on(name, once, context);
    },

    // Remove one or many callbacks. If `context` is null, removes all
    // callbacks with that function. If `callback` is null, removes all
    // callbacks for the event. If `name` is null, removes all bound
    // callbacks for all events.
    off: function(name, callback, context) {
      var retain, ev, events, names, i, l, j, k;
      if (!this._events || !eventsApi(this, 'off', name, [callback, context])) return this;
      if (!name && !callback && !context) {
        this._events = {};
        return this;
      }

      names = name ? [name] : _.keys(this._events);
      for (i = 0, l = names.length; i < l; i++) {
        name = names[i];
        if (events = this._events[name]) {
          this._events[name] = retain = [];
          if (callback || context) {
            for (j = 0, k = events.length; j < k; j++) {
              ev = events[j];
              if ((callback && callback !== ev.callback && callback !== ev.callback._callback) ||
                  (context && context !== ev.context)) {
                retain.push(ev);
              }
            }
          }
          if (!retain.length) delete this._events[name];
        }
      }

      return this;
    },

    // Trigger one or many events, firing all bound callbacks. Callbacks are
    // passed the same arguments as `trigger` is, apart from the event name
    // (unless you're listening on `"all"`, which will cause your callback to
    // receive the true name of the event as the first argument).
    trigger: function(name) {
      if (!this._events) return this;
      var args = slice.call(arguments, 1);
      if (!eventsApi(this, 'trigger', name, args)) return this;
      var events = this._events[name];
      var allEvents = this._events.all;
      if (events) triggerEvents(events, args);
      if (allEvents) triggerEvents(allEvents, arguments);
      return this;
    },

    // Tell this object to stop listening to either specific events ... or
    // to every object it's currently listening to.
    stopListening: function(obj, name, callback) {
      var listeners = this._listeners;
      if (!listeners) return this;
      var deleteListener = !name && !callback;
      if (typeof name === 'object') callback = this;
      if (obj) (listeners = {})[obj._listenerId] = obj;
      for (var id in listeners) {
        listeners[id].off(name, callback, this);
        if (deleteListener) delete this._listeners[id];
      }
      return this;
    }

  };

  // Regular expression used to split event strings.
  var eventSplitter = /\s+/;

  // Implement fancy features of the Events API such as multiple event
  // names `"change blur"` and jQuery-style event maps `{change: action}`
  // in terms of the existing API.
  var eventsApi = function(obj, action, name, rest) {
    if (!name) return true;

    // Handle event maps.
    if (typeof name === 'object') {
      for (var key in name) {
        obj[action].apply(obj, [key, name[key]].concat(rest));
      }
      return false;
    }

    // Handle space separated event names.
    if (eventSplitter.test(name)) {
      var names = name.split(eventSplitter);
      for (var i = 0, l = names.length; i < l; i++) {
        obj[action].apply(obj, [names[i]].concat(rest));
      }
      return false;
    }

    return true;
  };

  // A difficult-to-believe, but optimized internal dispatch function for
  // triggering events. Tries to keep the usual cases speedy (most internal
  // Backbone events have 3 arguments).
  var triggerEvents = function(events, args) {
    var ev, i = -1, l = events.length, a1 = args[0], a2 = args[1], a3 = args[2];
    switch (args.length) {
      case 0: while (++i < l) (ev = events[i]).callback.call(ev.ctx); return;
      case 1: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1); return;
      case 2: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2); return;
      case 3: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2, a3); return;
      default: while (++i < l) (ev = events[i]).callback.apply(ev.ctx, args);
    }
  };

  var listenMethods = {listenTo: 'on', listenToOnce: 'once'};

  // Inversion-of-control versions of `on` and `once`. Tell *this* object to
  // listen to an event in another object ... keeping track of what it's
  // listening to.
  _.each(listenMethods, function(implementation, method) {
    Events[method] = function(obj, name, callback) {
      var listeners = this._listeners || (this._listeners = {});
      var id = obj._listenerId || (obj._listenerId = _.uniqueId('l'));
      listeners[id] = obj;
      if (typeof name === 'object') callback = this;
      obj[implementation](name, callback, this);
      return this;
    };
  });

  // Aliases for backwards compatibility.
  Events.bind   = Events.on;
  Events.unbind = Events.off;

  // Mixin utility
  Events.mixin = function(proto) {
    var exports = ['on', 'once', 'off', 'trigger', 'stopListening', 'listenTo',
                   'listenToOnce', 'bind', 'unbind'];
    _.each(exports, function(name) {
      proto[name] = this[name];
    }, this);
    return proto;
  };

  // Export Events as BackboneEvents depending on current context
  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = Events;
    }
    exports.BackboneEvents = Events;
  }else if (typeof define === "function"  && typeof define.amd == "object") {
    define(function() {
      return Events;
    });
  } else {
    root.BackboneEvents = Events;
  }
})(this);

},{}],17:[function(require,module,exports){
module.exports = require('./backbone-events-standalone');

},{"./backbone-events-standalone":16}]},{},[4]);
