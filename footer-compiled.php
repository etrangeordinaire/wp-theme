		</main>

		<footer class="footer">
			
			<div class="footer-inner">
				<div class="footer-item">
					<div class="footer-heading">Contact</div>
					<div class="footer-description"><?= get_field('contact', 'option') ?></div>
				</div>
				<div class="footer-item">
					<div class="footer-heading">Réseaux sociaux</div>
					<div class="footer-description"><?= get_field('networks', 'option') ?></div>
				</div>
				<div class="footer-item">
					<div class="footer-heading">Prochaines dates</div>
					<div class="footer-description"><?= get_field('legal', 'option') ?></div>
				</div>
				
				<div class="footer-item">
					<div class="footer-heading">Liens utiles</div>
					<div class="footer-description"><?= get_field('links', 'option') ?></div>
			</div>
		</footer>

		<? get_view('lightbox') ?>

		
		<div id="svg-store"><!-- inject:svg --><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs>
		<rect id="SVGID_1_" y="0" width="16.001" height="14.941"/>
	</defs><symbol id="icon-available" viewBox="0 0 11.8 11.3">
<polygon points="5.9,0 7.3,4.3 11.8,4.3 8.2,7 9.6,11.3 5.9,8.6 2.3,11.3 3.7,7 0,4.3 
	4.5,4.3 "/>
</symbol><symbol id="icon-burger" viewBox="0 0 29 21">
    <!-- Generator: Sketch 39.1 (31720) - http://www.bohemiancoding.com/sketch -->
    <title>ico-burger</title>
    <desc>Created with Sketch.</desc>
    
    <g id="Symbols" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="navigation/header/full" transform="translate(-1350.000000, -92.000000)">
            <g id="Menu" transform="translate(820.000000, 0.000000)">
                <g id="Bt---burger" transform="translate(482.000000, 86.000000)">
                    <g id="icons/ico-burger" transform="translate(46.000000, 0.000000)">
                        <rect id="Rectangle-152" x="2.03137207" y="6" width="28.9372559" height="3" rx="1.5"/>
                        <rect id="Rectangle-152-Copy" x="2.03137207" y="15" width="28.9372559" height="3" rx="1.5"/>
                        <rect id="Rectangle-152-Copy-2" x="2.03137207" y="24" width="28.9372559" height="3" rx="1.5"/>
                    </g>
                </g>
            </g>
        </g>
    </g>
</symbol><symbol id="icon-close" viewBox="0 0 14.1 14.1">
<polygon points="11.8,0 7,4.8 2.3,0 0,2.3 4.8,7 0,11.8 2.3,14.1 7,9.3 11.8,14.1 
	14.1,11.8 9.3,7 14.1,2.3 "/>
</symbol><symbol id="icon-comment" viewBox="0 0 612 612">
<g>
	<g id="_x32__26_">
		<g>
			<path d="M401.625,325.125h-191.25c-10.557,0-19.125,8.568-19.125,19.125s8.568,19.125,19.125,19.125h191.25
				c10.557,0,19.125-8.568,19.125-19.125S412.182,325.125,401.625,325.125z M439.875,210.375h-267.75
				c-10.557,0-19.125,8.568-19.125,19.125s8.568,19.125,19.125,19.125h267.75c10.557,0,19.125-8.568,19.125-19.125
				S450.432,210.375,439.875,210.375z M306,0C137.012,0,0,119.875,0,267.75c0,84.514,44.848,159.751,114.75,208.826V612
				l134.047-81.339c18.552,3.061,37.638,4.839,57.203,4.839c169.008,0,306-119.875,306-267.75C612,119.875,475.008,0,306,0z
				 M306,497.25c-22.338,0-43.911-2.601-64.643-7.019l-90.041,54.123l1.205-88.701C83.5,414.133,38.25,345.513,38.25,267.75
				c0-126.741,119.875-229.5,267.75-229.5c147.875,0,267.75,102.759,267.75,229.5S453.875,497.25,306,497.25z"/>
		</g>
	</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</symbol><symbol id="icon-download" viewBox="0 0 34.2 34">
	<path d="M21.9,33h-9.6C5.5,33,0,27.5,0,20.7l0-8.5C0,5.5,5.5,0,12.3,0l9.6,0
		c6.8,0,12.3,5.5,12.3,12.3v8.5C34.2,27.5,28.7,33,21.9,33z"/>
	<rect x="11.1" y="18.7" style="fill:#FFFFFF;" width="12" height="15.2"/>
	<polygon style="fill:#FFFFFF;" points="7.8,23.1 17.1,7 26.4,23.1 	"/>
</symbol><symbol id="icon-external" viewBox="0 0 30 30.7">
<rect x="18" y="-2.9" transform="matrix(0.7071 0.7071 -0.7071 0.7071 13.4788 -9.8848)" width="1.4" height="28.5"/>
<rect x="17.8" width="12.1" height="1.4"/>
<rect x="28.6" width="1.4" height="12.1"/>
<polygon points="23.6,30.7 0,30.7 0,7.1 15.7,7.1 15.7,8.6 1.4,8.6 1.4,29.3 22.1,29.3 22.1,15 23.6,15 "/>
</symbol><symbol id="icon-less" viewBox="0 0 22.2 3.6">
<g id="XMLID_1_">
	<path id="XMLID_4_" d="M14.7,3.6h5.7c1,0,1.8-0.8,1.8-1.8v0c0-1-0.8-1.8-1.8-1.8h-5.7"/>
	<path id="XMLID_2_" d="M14.7,0H1.8C0.8,0,0,0.8,0,1.8v0c0,1,0.8,1.8,1.8,1.8h13.3"/>
</g>
</symbol><symbol id="icon-next" viewBox="0 0 9 15">
  <g>
    <path d="M10.3669181,7.89889695 L5.4171706,12.8486444 L7.22498852,14.6564623 L13.9746442,7.9068067 L6.77501148,0.707174022 L4.97510331,2.50708219 L10.3669181,7.89889695 Z" transform="translate(-5, 0)"/>
  </g>
</symbol><symbol id="icon-pending" viewBox="0 0 11.3 12.7">
	<path d="M5.6,11.7c0,0-0.2,0-0.6,0c-0.4,0-1-0.1-1.6-0.4c-0.6-0.3-1.3-0.7-1.9-1.3
		C0.9,9.4,0.4,8.5,0.2,7.6C0.1,7.2,0,6.7,0,6.2c0-0.4,0.1-0.9,0.2-1.4C0.4,4,0.8,3.2,1.2,2.7c0.5-0.6,0.9-1,1.3-1.2
		c0.4-0.2,0.6-0.3,0.6-0.3l0.4,0.8c0,0-0.2,0.1-0.5,0.3c-0.3,0.2-0.7,0.5-1.2,1C1.4,3.6,1,4.2,0.7,5C0.6,5.4,0.5,5.8,0.5,6.2
		c0,0.4,0,0.9,0.1,1.3c0.2,0.8,0.6,1.7,1.1,2.3c0.5,0.6,1.2,1.1,1.8,1.4c0.6,0.3,1.2,0.4,1.5,0.5C5.4,11.7,5.6,11.7,5.6,11.7z"/>
	<path d="M5.6,1c-1,0.7-2.2,1.7-2.8,2.7l0.2-2L1.8,0C2.9,0.5,4.4,0.9,5.6,1z"/>
	<path d="M5.6,1c0,0,0.2,0,0.6,0c0.4,0,1,0.1,1.6,0.4c0.6,0.3,1.3,0.7,1.9,1.3
		c0.6,0.6,1.1,1.5,1.3,2.4c0.1,0.5,0.2,0.9,0.2,1.4c0,0.4-0.1,0.9-0.2,1.4C10.9,8.7,10.5,9.5,10,10c-0.5,0.6-0.9,1-1.3,1.2
		c-0.4,0.2-0.6,0.3-0.6,0.3l-0.4-0.8c0,0,0.2-0.1,0.5-0.3c0.3-0.2,0.7-0.5,1.2-1c0.4-0.5,0.8-1.1,1.1-1.9c0.1-0.4,0.2-0.8,0.2-1.2
		c0-0.4,0-0.9-0.1-1.3c-0.2-0.8-0.6-1.7-1.1-2.3C9,2.3,8.4,1.8,7.8,1.5C7.2,1.2,6.6,1.1,6.2,1C5.9,1,5.6,1,5.6,1z"/>
	<path d="M5.6,11.7C6.6,11,7.8,10,8.5,9l-0.2,2l1.1,1.7C8.4,12.2,6.8,11.9,5.6,11.7z"/>
</symbol><symbol id="icon-people" viewBox="0 0 16.001 14.94">
<g>
	
	<clipPath id="SVGID_2_">
		<use xlink:href="#SVGID_1_" overflow="visible"/>
	</clipPath>
	<path clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" d="M3.204,12.869c0-2.761,2.239-5,5-5
		c2.761,0,5,2.239,5,5C13.204,15.631,3.204,15.631,3.204,12.869"/>
	<path clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" d="M8.204,1.572
		c1.545,0,2.797,1.252,2.797,2.797c0,1.545-1.252,2.797-2.797,2.797c-1.545,0-2.797-1.252-2.797-2.797
		C5.407,2.824,6.659,1.572,8.204,1.572"/>
	<path clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" d="M11.411,6.338
		c-0.238,0.379-0.522,0.727-0.874,1.004c2.153,0.912,3.667,3.045,3.667,5.527c0,0.021-0.005,0.04-0.006,0.061
		c1.069-0.342,1.803-0.886,1.803-1.633C16.001,8.676,13.978,6.55,11.411,6.338"/>
	<path clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" d="M2.204,12.869
		c0-2.48,1.514-4.615,3.666-5.528C5.507,7.055,5.213,6.695,4.972,6.299C2.224,6.315,0,8.545,0,11.296
		c0,0.842,0.931,1.425,2.22,1.752C2.215,12.988,2.204,12.932,2.204,12.869"/>
	<path clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" d="M12.001,4.369
		c0,0.382-0.075,0.742-0.183,1.089c1.144-0.352,1.979-1.403,1.979-2.662C13.798,1.252,12.546,0,11.001,0
		c-0.71,0-1.352,0.273-1.845,0.709C10.788,1.134,12.001,2.606,12.001,4.369"/>
	<path clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" d="M4.625,5.556
		C4.498,5.181,4.407,4.789,4.407,4.369c0-1.653,1.07-3.049,2.549-3.568C6.451,0.306,5.762,0,5,0C3.456,0,2.204,1.252,2.204,2.797
		C2.204,4.213,3.259,5.371,4.625,5.556"/>
</g>
</symbol><symbol id="icon-pin" viewBox="0 0 13.1 20.9">
	<path d="M6.5,0C2.9,0,0,2.9,0,6.5c0,6.2,6.5,14.4,6.5,14.4s6.5-8.1,6.5-14.4
		C13.1,2.9,10.1,0,6.5,0z M6.5,10.1C4.6,10.1,3,8.6,3,6.6c0-1.9,1.6-3.5,3.5-3.5c1.9,0,3.5,1.6,3.5,3.5C10.1,8.6,8.5,10.1,6.5,10.1z
		"/>
</symbol><symbol id="icon-plus" viewBox="0 0 34.6 34.6">
<path d="M31.8,14.5h-8.9c-1.5,0-2.8-1.3-2.8-2.8V2.8c0-1.5-1.3-2.8-2.8-2.8h0c-1.5,0-2.8,1.3-2.8,2.8v8.9
	c0,1.5-1.3,2.8-2.8,2.8H2.8c-1.5,0-2.8,1.3-2.8,2.8v0c0,1.5,1.3,2.8,2.8,2.8h8.9c1.5,0,2.8,1.3,2.8,2.8v8.9c0,1.5,1.3,2.8,2.8,2.8h0
	c1.5,0,2.8-1.3,2.8-2.8v-8.9c0-1.5,1.3-2.8,2.8-2.8h8.9c1.5,0,2.8-1.3,2.8-2.8v0C34.6,15.7,33.3,14.5,31.8,14.5z"/>
</symbol><symbol id="icon-preparing" viewBox="0 0 10 8.7">
<polygon points="0,8.7 5,0 10,8.7 "/>
</symbol><symbol id="icon-votedown" viewBox="80.5 -24.4 456.1 448.8">
<path stroke-width="31" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M235.3,170.1c0,13.6,0,27.1,0,40.7c1.6,2,3.1,3.9,4.7,5.9c6.2,7.7,12.6,15.7,18,24.5c9.4,15.1,18.3,30.7,27,45.8
	c3.5,6.2,7.2,12.6,10.9,18.9c3.5,6,5.2,13,4.8,20c-0.4,10.1-0.6,33.1-0.8,43.1c-0.1,6-0.2,12.2-0.4,18.3c-0.2,7.4,3,12.6,10.1,16.4
	c12.4,6.5,25.1,6.9,38.8,1.1c5.7-2.4,9.3-6.2,11.7-12.6c1.5-3.9,3-7.9,4.5-11.9c4-10.5,8.1-34.1,11.8-44.8
	c4.6-13.3,5.1-27.8,1.6-45.6c-1.6-8.5-3-17.2-4.4-25.6c-0.6-4.2-1.3-8.4-2-12.5c-0.5-3-1.2-7.5,2.1-11.4c3.3-3.9,7.9-3.9,10.9-3.9
	h27.9c24.8,0.1,50.5,0.1,75.7-0.1c5.5,0,11-0.8,16.2-2.5c8-2.8,13.1-9,15.7-19.1l0,0c0.3-1.2,0.7-2.4,1-3.5v-9.1
	c-0.1-0.5-0.3-1-0.4-1.5v-0.2c-1.6-8.2-4.7-14.9-9.5-20.2c-3.9-4.4-2.9-10.1-2.5-12c0.5-2.2,1-4.4,1.5-6.4c0.9-2.9,1.5-6,1.7-9
	c0.3-9-2.8-17.4-9.5-25.8c-3.3-4-4.2-9.5-2.4-14.4c1.2-3.8,2.5-7.7,2.7-11.1c0.6-9.8-2.5-19.3-9.7-28.8c-2.2-2.9-4.1-7.9-2.3-12.7
	c2-5.3,2.6-11.2,2-19.1c-1.2-16.7-10.9-28.9-27.2-34.2c-9.1-3-18.6-3.4-27.4-3.4h-46.9c-42.6,0-86.7,0.1-130.1-0.2
	c-9-0.2-17.9,1.8-25.9,5.9C235.5,56.1,235.6,107.5,235.3,170.1z"/>
<path stroke-width="31" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M96,11.8v197c0,11.4,9.2,20.7,20.7,20.7h98c11.4,0,20.7-9.2,20.7-20.7v-197c0-11.4-9.2-20.7-20.7-20.7h-98C105.3-8.9,96,0.4,96,11.8
	z"/>
<circle stroke-width="20" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="165.3" cy="59" r="29.7"/>
</symbol><symbol id="icon-voteup" viewBox="80.5 -24.4 456.1 448.8">
<path stroke-width="31" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M235.2,390.9c8,4.1,16.9,6.1,25.9,5.9c43.4-0.3,87.5-0.2,130.1-0.2h46.9c8.8,0,18.3-0.4,27.4-3.4c16.3-5.3,26-17.5,27.2-34.2
	c0.6-7.9,0-13.8-2-19.1c-1.8-4.8,0.1-9.8,2.3-12.7c7.2-9.5,10.3-19,9.7-28.8c-0.2-3.4-1.5-7.3-2.7-11.1c-1.8-4.9-0.9-10.4,2.4-14.4
	c6.7-8.4,9.8-16.8,9.5-25.8c-0.2-3-0.8-6.1-1.7-9c-0.5-2-1-4.2-1.5-6.4c-0.4-1.9-1.4-7.6,2.5-12c4.8-5.3,7.9-12,9.5-20.2v-0.2
	c0.1-0.5,0.3-1,0.4-1.5v-9.1c-0.3-1.1-0.7-2.3-1-3.5l0,0c-2.6-10.1-7.7-16.3-15.7-19.1c-5.2-1.7-10.7-2.5-16.2-2.5
	c-25.2-0.2-50.9-0.2-75.7-0.1h-27.9c-3,0-7.6,0-10.9-3.9c-3.3-3.9-2.6-8.4-2.1-11.4c0.7-4.1,1.4-8.3,2-12.5
	c1.4-8.4,2.8-17.1,4.4-25.6c3.5-17.8,3-32.3-1.6-45.6c-3.7-10.7-7.8-34.3-11.8-44.8c-1.5-4-3-8-4.5-11.9c-2.4-6.4-6-10.2-11.7-12.6
	c-13.7-5.8-26.4-5.4-38.8,1.1c-7.1,3.8-10.3,9-10.1,16.4c0.2,6.1,0.3,12.3,0.4,18.3c0.2,10,0.4,33,0.8,43.1c0.4,7-1.3,14-4.8,20
	c-3.7,6.3-7.4,12.7-10.9,18.9c-8.7,15.1-17.6,30.7-27,45.8c-5.4,8.8-11.8,16.8-18,24.5c-1.6,2-3.1,3.9-4.7,5.9c0,13.6,0,27.1,0,40.7
	C235.6,292.5,235.5,343.9,235.2,390.9z"/>
<path stroke-width="31" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M116.7,408.9h98c11.5,0,20.7-9.3,20.7-20.7v-197c0-11.5-9.3-20.7-20.7-20.7h-98c-11.5,0-20.7,9.3-20.7,20.7v197
	C96,399.6,105.3,408.9,116.7,408.9z"/>
<circle stroke-width="20" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="165.3" cy="341" r="29.7"/>
</symbol></svg><!-- endinject --></div>

		<!--[if lte IE 9]>
		<? get_view('ie10'); ?>
		<![endif]-->
		
		<? wp_footer() ?>
		
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/jquery.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/lodash.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/slick.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/tweenmax.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/scrollmagic.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/debug.addIndicators.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/assets/js/animation.gsap.min.js"></script>
		<script src="<?= get_stylesheet_directory_uri() ?>/dist/app.js?noCache=97ab3430"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKSFTHZadCKrGrWOWgpLkHVJq_2iarYY8&callback=initMap"></script>
	</body>
</html>