<?


/* Debug Helpers */
/* --------------------------------------------------------------------------------- */


/*
 * var_dump amélioré
 */

function var_debug($var) {
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}


/* Template Helpers */
/* --------------------------------------------------------------------------------- */

function icon($id, $class = '', $inline = false) {
  if($inline) {
    
  } else
    echo '<svg class="icon '.$class.'"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-'.$id.'"></use></svg>';
}

function animatesvg($id) {
  ?>
  <div class="animatesvg">
    <? include(TEMPLATEPATH.'/assets/images/'.$id.'.svg'); ?>
  </div>
  <?
}


function showDate($item) {
  $date = get_field('fixed_date', $item->ID);
  ?>
  <div class="date">
    <?
    if(get_field('approx_display', $item->ID)):
      ?>
      <div class="date-approx"><?= get_field('approx_date', $item->ID) ?></div>
      <?
      else:
      ?>
      <div class="date-fixed">
        <div class="date-date">
          <span class="date-day"><?= date_i18n('j F', $date) ?></span>
          <span class="date-year">&nbsp;<?= date_i18n('Y', $date) ?></span>
        </div>
        <div class="date-time"><?= date_i18n('H:i', $date) ?></div>
      </div>
      <?
    endif;
    ?>
  </div>
  <?
}

// user full name
function getFullName($userId) {
  $data = get_userdata($userId);
  return $data->first_name . ' ' . $data->last_name;
}

// user full name
function getShortName($userId) {
  $data = get_userdata($userId);
  return $data->first_name[0] . '. ' . $data->last_name;
}

// careful with security here
function get_view($name) {
	include(TEMPLATEPATH.'/views/'.$name.'.php');
}


/* 
 * truncate string 
 */

function tokenTruncate($string, $your_desired_width, $end_string = '...') {
  $string = strip_tags($string);
  $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
  $parts_count = count($parts);

  $has_been_cut = false;
  $length = 0;
  $last_part = 0;
  for (; $last_part < $parts_count; ++$last_part) {
    $length += strlen($parts[$last_part]);
    if ($length > $your_desired_width) { 
      $has_been_cut = true;
      break; 
    }
  }
  
  // if cut add $end_string
  if($has_been_cut) {
    return implode(array_slice($parts, 0, $last_part)).$end_string;
  } else {
    return implode(array_slice($parts, 0, $last_part));
  }
}


/* AJAX Helpers */
/* --------------------------------------------------------------------------------- */

// global > return json response from array
function returnResponse($response_array, $code = 200) {
  $response = json_encode($response_array, JSON_HEX_QUOT);
  headerCode($code);
  echo $response;
  exit;
}

function headerCode($code) {
  http_response_code($code);
  header("HTTP/1.0 ".$code);
  header("Content-Type: application/json");
}
?>
