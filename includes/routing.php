<?php

/*
 * Custom routing
 */

add_action( 'wp_router_generate_routes', 'bl_add_routes', 20 );
function bl_add_routes( $router ) {
	generate_route( [ 'url' => 'ie10', 'slug' => 'pages/ie10' ] , $router);
	generate_route( [ 'url' => 'clear-cache', 'slug' => 'pages/clear-cache'] , $router);
	generate_route( [ 'url' => 'login', 'slug' => 'pages/user/login'] , $router);
	generate_route( [ 'url' => 'signup', 'slug' => 'pages/user/signup'] , $router);
	generate_route( [ 'url' => 'signed', 'slug' => 'pages/user/signed'] , $router);

	// API
	generate_route( [ 'url' => 'api/tweets' ] , $router);
	generate_route( [ 'url' => 'api/contact/send' ] , $router);
	generate_route( [
				'url' => 'api/proposition/vote/(.*?)/(.*?)',
				'slug' => 'api/proposition/vote',
				'query_vars' =>  [
						'item_id' => 1,
						'vote' => 2
				]
			]
			, $router);

	generate_route( [
				'url' => 'api/expression/like/(.*?)',
				'slug' => 'api/expression/like',
				'query_vars' =>  [
					'item_id' => 1,
				]
			]
			, $router);

	generate_route( [
				'url' => 'api/comment/(.*?)',
				'slug' => 'api/comment',
				'query_vars' =>  [
					'item_id' => 1,
				]
			]
			, $router);


}


/*
 * Generate route
 */

function generate_route($args, $router) {

	$url   = $args['url'];
	if($url==null) return false;

	$slug       = (isset($args['slug']))?$args['slug']:$args['url'];
	$title      = (isset($args['title']))?$args['title']:$args['url'];
	$query_vars = (isset($args['query_vars']))?$args['query_vars']:[];

	$route_args = [
									'path'            => '^'.$url.'$',
									'query_vars'      => array_merge($query_vars, ['route'=>$slug]),
									'page_callback'   => 'empty_callback',
									'page_arguments'  => array_keys($query_vars),
									'access_callback' => true,
									'title'           => $title,
									'template'        => [
										$slug.'.php',
										TEMPLATEPATH . '/'.$slug.'.php'
									]
								];
	$router->add_route( $slug, $route_args );
}


/*
 * Is route
 */

function is_route($route) {
	return get_query_var('route') == $route;
}

/*
 * Empty callback
 */
function empty_callback( ) { return ''; }
?>
