<?php

/*
 * Main theme setup
 */

add_action( 'after_setup_theme', 'blank_theme_setup' );
function blank_theme_setup() {
	add_theme_support( 'post-thumbnails' );
	add_action( 'init', 'register_all' );

  remove_image_size('large');
}

function register_all() {
  register_nav_menus(
    array(
      'header' => __('Header', 'blank'),
      'mobile' => __('Mobile', 'blank')
    )
  );

  register_post_type('veille',
      array(
      'labels' => array(
        'name' => __('Veille', 'blank'),
        'singular_name' => __('Article', 'blank'),
        'add_new' => __('Ajouter article', 'blank'),
        'add_new_item' => __('Ajouter nouvel article', 'blank'),
        'edit' => __('Editer article', 'blank')
      ),
      'public' => true,
      'publicly_queryable' => true,
      'hierarchical' => false,
      'has_archive' => false,
      'supports' => array( 'title' ),
      'can_export' => true
    ));

  register_taxonomy(
      'dossier',
      ['veille'],
      array(
        'label' => __( 'Dossiers', 'blank' ),
        'rewrite' => array( 'slug' => 'dossier' ),
        'hierarchical' => true,
        'show_admin_column' => true,
      ));

  register_post_type('proposition',
      array(
      'labels' => array(
        'name' => __('Propositions', 'blank'),
        'singular_name' => __('Proposition', 'blank'),
        'add_new' => __('Ajouter item', 'blank'),
        'add_new_item' => __('Ajouter nouvel item', 'blank'),
        'edit' => __('Editer item', 'blank')
      ),
      'public' => true,
      'publicly_queryable' => false,
      'hierarchical' => false,
      'has_archive' => false,
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
      'can_export' => true
    ));

  register_taxonomy(
      'sondage',
      ['proposition'],
      array(
        'label' => __( 'Sondages', 'blank' ),
        'rewrite' => array( 'slug' => 'sondage' ),
        'hierarchical' => true,
        'show_admin_column' => true,
      ));

  register_taxonomy(
      'axe',
      ['proposition'],
      array(
        'label' => __( 'Axes', 'blank' ),
        'rewrite' => array( 'slug' => 'axe' ),
        'hierarchical' => true,
        'show_admin_column' => true,
      ));

  register_post_type('expression',
      array(
      'labels' => array(
        'name' => __('Expression', 'blank'),
        'singular_name' => __('Expression', 'blank'),
        'add_new' => __('Ajouter item', 'blank'),
        'add_new_item' => __('Ajouter nouvel item', 'blank'),
        'edit' => __('Editer item', 'blank')
      ),
      'public' => true,
      'publicly_queryable' => false,
      'hierarchical' => false,
      'has_archive' => false,
      'supports' => array( 'title', 'comments' ),
      'can_export' => true
    ));

  register_post_type('timeline',
      array(
      'labels' => array(
        'name' => __('Timeline', 'blank'),
        'singular_name' => __('Timeline', 'blank'),
        'add_new' => __('Ajouter item', 'blank'),
        'add_new_item' => __('Ajouter nouvel item', 'blank'),
        'edit' => __('Editer item', 'blank')
      ),
      'public' => true,
      'publicly_queryable' => true,
      'hierarchical' => true,
      'has_archive' => false,
      'supports' => array( 'title', 'page-attributes' ),
      'can_export' => true
    ));

  register_post_type('intervenant',
      array(
      'labels' => array(
        'name' => __('Intervenants', 'blank'),
        'singular_name' => __('Intervenant', 'blank'),
        'add_new' => __('Ajouter item', 'blank'),
        'add_new_item' => __('Ajouter nouvel item', 'blank'),
        'edit' => __('Editer item', 'blank')
      ),
      'public' => true,
      'publicly_queryable' => false,
      'hierarchical' => false,
      'has_archive' => false,
      'supports' => array( 'title' ),
      'can_export' => true
    ));

}

add_filter('intermediate_image_sizes_advanced', function($sizes) {
    unset( $sizes['large']);
    return $sizes;
});

add_filter( 'use_default_gallery_style', '__return_false' );

add_filter('upload_mimes', function($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
});

add_filter('wp_mail_from', function($content_type) {
  return get_option( 'admin_email' );
});

add_filter('wp_mail_from_name', function($name) {
  return get_option('blogname');
});