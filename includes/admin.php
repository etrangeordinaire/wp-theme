<?
use \Blank\Content;
use phpFastCache\CacheManager;


/**
 * User roles
 */

if(get_role('author')) remove_role('author');
if(get_role('editor')) remove_role('editor');
if(get_role('contributor')) remove_role('contributor');
if(!get_role('participant')) add_role('participant', __( 'Participant' ), [ 'read' => true ]);



/**
 * Menu pages
 */

add_action( 'admin_menu', function() {
	remove_menu_page('edit.php');
	remove_menu_page( 'edit-comments.php' );
});

add_filter('acf/options_page/settings', function($acf_options_page_settings) {
  $acf_options_page_settings['capability'] = 'edit_themes';
  return $acf_options_page_settings;
});


/*
 * Admin JS/CSS
 */

add_action('admin_head', function() {
  echo '<script type="text/javascript" src="'. get_bloginfo('template_directory') . '/assets/js/admin.js"></script>';
  echo '<link rel="stylesheet" href="'.        get_bloginfo('template_directory') . '/assets/css/admin.css" />';
});


/**
 * Custom column
 */

add_filter('manage_edit-timeline_columns', function($columns) {
    $columns['type'] = "Type";
    $columns['date_event'] = "Date de l'événement";
    return $columns;
});

add_action('manage_timeline_posts_custom_column',  function($name) {
    global $post;
    switch ($name) {
        case 'type': echo get_field('type', $post->ID); break;
        case 'date_event': 
          if('event' != get_field('type', $post->ID)) break;
          if(get_field('approx_display', $post->ID))
            echo get_field('approx_date', $post->ID) . ' <br>fixe : ';
          echo date_i18n('j F Y à H:i', get_field('fixed_date', $post->ID)); 
          break;
    		
    }
});

add_filter('manage_posts_columns', function($defaults) {
  unset($defaults['date']);
  return $defaults;
});

add_action( 'admin_menu', function() {
  remove_meta_box( 'sondagediv', 'proposition', 'side');
  remove_meta_box( 'axediv', 'proposition', 'side');
});

add_action( 'save_post', function($post_id) {
  global $post;

  if ( wp_is_post_revision( $post_id ) ) return;

  if('veille' == $post->post_type) {
    global $veille;
    $InstanceCache = CacheManager::getInstance('files');
    $CachedOG = $InstanceCache->getItem('veille_'.$post->ID);

    if( !is_null($CachedOG->get()) ) {
      $CachedOG->expiresAfter(1);
      $InstanceCache->save($CachedOG);
    }
  }
});


