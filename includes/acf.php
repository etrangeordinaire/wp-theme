<?php

/*
 * ACF - Register options
 */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'menu_slug' 	=> 'theme-footer-settings',
		'capability'	=> 'edit_themes',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'theme-options-settings',
		'capability'	=> 'edit_themes',
		'redirect'		=> false
	));
}