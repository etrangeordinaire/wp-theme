<?

/**
 * Template Name: Sondages
 */

global $post;
get_header('compiled');
get_view('hero');
?>
<div class="page-inner">
	<div class="sondages">
		<?
		$cases = [
			['actif' => '1' , 'label' => "En cours..."],
			['actif' => '0' , 'label' => "Passé"]
		];
		foreach($cases as $case):
			?>
			<h2 class="sondages-title"><?= $case['label'] ?></h2>
			<div class="sondages-list">
				<?
				$sondages = get_terms([
					'taxonomy' => 'sondage',
					'meta_query' => [[
						'key' => 'actif',
						'value' => $case['actif'],
						// 'type' => 'BINARY'
					]]
					]);
				foreach($sondages as $sondage):
					?>
					<a class="sondages-item" href="<?= get_term_link($sondage) ?>">
						<div class="sondages-item-main">
							<h3 class="sondages-item-title"><?= $sondage->name ?></h3>
							<div class="sondages-item-subtitle">
								<?= get_field('subtitle', 'sondage_'.$sondage->term_id) ?>
							</div>
						</div>
						<span class="sondages-item-cta"><?= icon('next') ?></span>
					</a>
					<?
				endforeach;
				?>
			</div>
			<?
		endforeach;
		?>
	</div>
</div>
<? get_footer('compiled'); ?>