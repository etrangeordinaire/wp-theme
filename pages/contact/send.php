<?
	
// set from form
$name = $_REQUEST['name'];
$email = $_REQUEST['email'];
$phone = $_REQUEST['phone'];
$subject = $_REQUEST['subject'];
$message = $_REQUEST['message'];

// validate
$errors = [];
if(!$name) $errors['name'] = 'Ce champs est requis.';
if(!$email) $errors['email'] = 'Ce champs est requis.';
elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) $errors['email'] = "L'email est invalide.";
if(!$subject) $errors['subject'] = 'Ce champs est requis.';
if(!$message) $errors['message'] = 'Ce champs est requis.';
if(!empty($errors)) {
	returnResponse($errors, 400);
	exit;
}

$infoMessage = "
Nom: ".$name." \n
Email: ".$email." \n
Téléphone: ".$phone." \n
Subject: ".$subject." \n
Message: ".$message." \n
";

// send mail to Admin
wp_mail(get_option( 'admin_email' ), "Nouveau message depuis le site", $infoMessage);

// send mail to Client
$clientMessage = "
Bonjour ".$name.",\n
Nous avons bien reçu votre demande, notre équipe s'engage à vous répondre dans un délai de 24h!\n
Vous trouverez ci-dessous l'information que vous nous avez transmise.\n
Merci,\n
L'équipe étrangeOrdinaire\n\n
---------------------------------------------------------------------------------\n\n
Votre Message :\n
".$infoMessage;

wp_mail($email, "Nous avons bien reçu votre message", $clientMessage);

// success
$successMessage = "
	<p>
		Votre message a bien été envoyé,
		<br><br>
		Vous venez de recevoir une copie de votre demande dans votre boîte email,
		vérifiez dans les courriers indésirables pour autoriser nos communications futures.
		<br><br>
		Nous nous engageons à vous répondre dans un délai de 24h,
		<br><br>
		Merci,
		<br>
		L'équipe étrangeOrdinaire!
	</p>
";

returnResponse(['message' => $successMessage]);
?>