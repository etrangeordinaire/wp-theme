<?

/**
 * Template Name: Équipe
 */

global $post;
get_header('compiled');
get_view('hero');
?>
<div class="page-inner">
	<div class="previews is-active">
		<?
		$users = get_users([
			'role__not_in' => 'subscriber'
			]);
		foreach($users as $user):
			?>
			<a class="preview" href="<?= site_url() ?>/profile/<?= $user->ID ?>">
				<div class="preview-header">
					<h3 class="preview-title"><?= getFullName($user->ID) ?></h3>
					<h4 class="preview-subtitle"><?= get_field('fonction', 'user_'.$user->ID) ?></h4>
				</div>
				<!-- <span class="preview-more" href="<?= get_permalink($veille->ID) ?>">En savoir plus</span> -->
			</a>
			<?
		endforeach;
		?>
		<a class="preview preview-add" href="<?= site_url() ?>/signup"><? icon('plus') ?>Inscrivez-vous</a>
	</div>
</div>
<? get_footer('compiled'); ?>