<?

/**
 * Template Name: Calendrier
 */

global $post;
get_header('compiled');
get_view('hero');
?>
<div class="calendrier">
	<?
	$today = date('Ymd');
	$nextDay = null;

	// recursivily show items
	function showItems($items) {
		global $today, $nextDay;

		foreach($items as $key => $item):
			$type = get_field('type', $item->ID);

			if(in_array($type, ['phase','etape'])) {
				$children = get_posts([
						'post_type' => 'timeline',
						'posts_per_page' => -1,
						'order' => 'ASC',
						'orderby' => 'menu_order',
						'post_parent' => $item->ID
					]);
			}
			
			switch($type):
				case 'phase':
					?>
					<div class="calendrier-item calendrier-phase" id="phase-<?= $key ?>">
						<h2 class="calendrier-phase-title" data-toggleClass="collapsed" data-targetId="phase-<?= $key ?>">
							<span class="calendrier-phase-more"><? icon('plus') ?></span>
							<span class="calendrier-phase-less"><? icon('less') ?></span>
							<?= $item->post_title ?>
							<div class="calendrier-phase-moretext">
								Il y a <?= count($children) ?> événement(s) dans cette phase,<br> 
								Cliquez pour voir les résultats.
							</div>
						</h2>
						
						<? if($children): ?>
						<div class="calendrier-children"><? showItems($children); ?></div>
						<? endif; ?>
					</div>
					<?
				break;

				case 'etape':
					?>
					<div class="calendrier-item calendrier-etape">
						<h2 class="calendrier-etape-title"><?= $item->post_title ?></h2>
						<? if($children): ?>
						<div class="calendrier-children"><? showItems($children); ?></div>
						<? endif; ?>
					</div>
					<?
				break;

				case 'event':
					$status = get_field('status', $item->ID);
					$hasSingle = get_field('has_single', $item->ID);

					// set nextDay
					$day = date('Ymd', get_field('fixed_date', $item->ID));
					if (!$nextDay && $day >= $today || $day >= $today && $day < $nextDay) 
						$nextDay = $day;
					
					$time = $day >= $today ? 'future' : 'past'
					?>

					<div class="calendrier-item calendrier-event status-<?= $status['value'] ?> time-<?= $time ?>" <?= $hasSingle?'data-href="'.get_permalink($item->ID).'"':'' ?> data-day="<?= $day ?>">
						<? 
						showDate($item);
						?>
						<div class="calendrier-event-main">
							<h4 class="calendrier-event-title">
								<?= $item->post_title ?>
								<?if('void' != $status['value']): ?>
								<span class="calendrier-event-status"><? icon($status['value']) ?> <?= $status['label'] ?></span>
								<? endif; ?>
							</h4>
							<h5 class="calendrier-event-subtitle"><?= get_field('subtitle', $item->ID) ?></h5>
							
							<div class="calendrier-event-metas">
								<? if(get_field('place', $item->ID)): ?>
								<div class="calendrier-event-meta">
									<? icon('pin') ?>
									<?= get_field('place', $item->ID) ?>
								</div>
								<? endif; ?>

								<? if(get_field('team', $item->ID)): ?>
								<div class="calendrier-event-meta">
									<? icon('people') ?>
									<?= get_field('team', $item->ID) ?>
								</div>
								<? endif; ?>
							</div>


							<? if($hasSingle): ?>
							<a class="calendrier-event-more" href="<?= get_permalink($item->ID) ?>">
								Voir le détail <span>&rarr;</span>
							</a>
							<? endif; ?>
						</div>
					</div>

					<?
				break;
			endswitch;
			?>
			
			<?
		endforeach;
	}
	
	// root elements
	$items = get_posts([
		'post_type' => 'timeline',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'orderby' => 'menu_order',
		'post_parent' => 0
		]);
	showItems($items);
?>


<script type="text/javascript">
var nextDay = <?= $nextDay ? $nextDay : 'null' ?>;
var today = <?= $today ?>;

// highlight nextDay and lowlight future
var evenements = document.querySelectorAll('.calendrier-event.time-future');
evenements.forEach(function(evenement) {
	if(nextDay == evenement.getAttribute('data-day')) evenement.classList.add('next');
})

// collapse past phase
var phases = document.querySelectorAll('.calendrier-phase');
phases.forEach(function(phase) {
	var id = phase.getAttribute('id');
	var children = document.querySelectorAll('#'+id+' .calendrier-event');
	var isPast = true;
	children.forEach(function(child) {
		console.log(child);
		if(child.classList.contains('time-future')) isPast = false;
	});
	if(isPast) phase.classList.add('collapsed');
})
</script>
<? get_footer('compiled'); ?>