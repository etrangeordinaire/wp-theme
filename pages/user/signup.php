<?
// validate signup
if($_SERVER['REQUEST_METHOD'] == 'POST') {

	$firstName = $_REQUEST['firstName'];
	$lastName = $_REQUEST['lastName'];
	$title = $_REQUEST['title'];
	$email = $_REQUEST['email'];
	$password = $_REQUEST['password'];

	$errors = [];
	if(!$firstName) $errors['firstName'] = "Ce champs est obligatoire";
	if(!$lastName) $errors['lastName'] = "Ce champs est obligatoire";
	if(!$title) $errors['title'] = "Ce champs est obligatoire";
	if(!$email) $errors['email'] = "Ce champs est obligatoire";
	elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) $errors['email'] = "Cet email n'est pas valide";
	if(strlen($password) < 8) $errors['password'] = "Doit contenir 8 caractères minimum";
	

	// field OK => create account
	if(!$errors) {
		$userId = wp_create_user($email, $password, $email);
		if(is_wp_error($userId)) $errors['email'] = "Cet email est déjà utilisé";	
		else {
			// set meta
			update_user_meta($userId, 'first_name', ucfirst($firstName));
			update_user_meta($userId, 'last_name', ucfirst($lastName));
			update_user_meta($userId, 'fonction', ucfirst($title));

			// redirect
			wp_set_auth_cookie($userId, false);
			wp_redirect(site_url().'/signed');
		}
	}
}

get_header('compiled');
?>
<div class="user">
	<div class="page-inner">
		<h1 class="user-title">Inscription</h1>

		<form class="form" action="<?= site_url() ?>/signup" method="POST">
			
			<? if($errors): ?>
			<div class="form-message">Veuillez corriger les erreurs ci-dessous</div>
			<? endif; ?>

			<div class="form-field <?= $errors['firstName']?'form-error':'' ?>">
				<div class="form-error-msg"><?= $errors['firstName'] ?></div>
				<input class="form-input" name="firstName" type="text" placeholder="Prénom" value="<?= $firstName ?>">
			</div>

			<div class="form-field <?= $errors['lastName']?'form-error':'' ?>">
				<div class="form-error-msg"><?= $errors['lastName'] ?></div>
				<input class="form-input" name="lastName" type="text" placeholder="Nom" value="<?= $lastName ?>">
			</div>

			<div class="form-field <?= $errors['title']?'form-error':'' ?>">
				<div class="form-error-msg"><?= $errors['title'] ?></div>
				<input class="form-input" name="title" type="text" placeholder="Titre, poste, fonction..." value="<?= $title ?>">
			</div>

			<div class="form-field <?= $errors['email']?'form-error':'' ?>">
				<div class="form-error-msg"><?= $errors['email'] ?></div>
				<input class="form-input" name="email" type="email" placeholder="Email" value="<?= $email ?>">
			</div>

			<div class="form-field <?= $errors['password']?'form-error':'' ?>">
				<div class="form-error-msg"><?= $errors['password'] ?></div>
				<input class="form-input" name="password" type="password" placeholder="Mot de passe">
			</div>
			<button class="form-button">Envoyer</button>
		</form>
		
		<!-- <div class="user-links">
			<a href="<?= site_url() ?>/login">J'ai déjà un compte</a>
		</div> -->
	</div>
</div>
<?
get_footer('compiled');
?>