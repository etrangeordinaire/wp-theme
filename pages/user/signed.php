<?
get_header('compiled');
?>
<div class="user">
	<div class="page-inner">
		<h1 class="user-title">Félicitation</h1>

		<div class="user-description">
			Vous êtes bien inscris sur le site! <br>
			Vous recevrez une approbation par email de la part de notre équipe sous peu.
		</div>
	</div>
</div>
<?
get_footer('compiled');
?>