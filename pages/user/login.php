<?
// validate login
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$userLogin = wp_authenticate($_REQUEST['username'], $_REQUEST['password']);
	$errorMessage = '';
	if($userLogin instanceof WP_Error) $errorMessage = 'Identifiants non valides';
	else {
		wp_set_auth_cookie($userLogin->ID, false);
		wp_redirect(site_url());
	}
}

get_header('compiled');
?>
<div class="user">
	<div class="page-inner">
		<h1 class="user-title">Connexion</h1>

		<form class="form" action="<?= site_url() ?>/login" method="POST">
			
			<? if($errorMessage): ?>
			<div class="form-message"><?= $errorMessage ?></div>
			<? endif; ?>

			<div class="form-field <?= $errorMessage?'form-error':'' ?>">
				<input class="form-input" name="username" type="email" placeholder="Email">
			</div>
			<div class="form-field <?= $errorMessage?'form-error':'' ?>">
				<input class="form-input" name="password" type="password" placeholder="Password">
			</div>

			<button class="form-button">Envoyer</button>
		</form>

		<div class="user-links">
			<a href="<?= site_url() ?>/password">Mot de passe oublié ?</a>
			-
			<a href="<?= site_url() ?>/signup">S'inscrire</a>
		</div>
	</div>
</div>
<?
get_footer('compiled');
?>