<?
if(!is_user_logged_in()) wp_redirect(site_url().'/login');
elseif(!user_can('edit_posts')) wp_redirect(site_url().'/403');
?>