<?

/**
 * Template Name: Actualites
 */

global $post;
get_header('compiled');
get_view('hero');
?>
<div class="page-inner actualites">

	<?
	if(get_field('fb_posts')):
		?>
		<div class="facebook">
			<h2 class="actualites-title">Sur Facebook</h2>
			<div class="facebook-list">
				<?
				foreach(get_field('fb_posts') as $fbPost):
					?>
					<div class="facebook-post"><?= $fbPost['fb_post'] ?></div>
					<?
				endforeach;
				?>
			</div>
		</div>
		<?
	endif;
	?>

	<?
	if(get_field('hashtag')):
		?>
		<div class="tweets">
			<h2 class="actualites-title">Sur Twitter</h2>
			<div class="tweets-list"></div>
			<a class="tweets-more is-loading">
				<button class="tweets-more-button">Afficher plus</button>
				<img class="tweets-more-loader" src="<?= get_stylesheet_directory_uri() ?>/assets/images/loader.svg">
			</a>
		</div>
		<script type="text/javascript">
		var hashtag = '#<?= get_field('hashtag') ?>';
		</script>
		<?
	endif;
	?>
	
</div>

<? get_footer('compiled'); ?>