<?

/**
 * Template Name: Expression
 */

// retrieve user votes
global $wpdb;
session_start();
$query = $wpdb->prepare('SELECT * FROM eopro_expression_like WHERE session_id LIKE %s', session_id());
$results = $wpdb->get_results($query);
$userLikes = [];
foreach($results as $userLike) $userLikes[$userLike->item_id] = $userLike;


// validate form
if($_SERVER['REQUEST_METHOD'] == 'POST') {

	$title = $_REQUEST['title'];
	$description = $_REQUEST['description'];
	$email = $_REQUEST['email'];
	$formname = $_REQUEST['formname'];

	$errors = [];
	if(!$formname) $errors['formname'] = "Ce champs est obligatoire";
	if(!$title) $errors['title'] = "Ce champs est obligatoire";
	if(!$description) $errors['description'] = "Ce champs est obligatoire";
	if(!$email) $errors['email'] = "Ce champs est obligatoire";
	elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) $errors['email'] = "Cette adresse n'est pas valide";
	

	// field OK => create
	if(!$errors) {
		$postId = wp_insert_post([
			'post_author' => 0,
			'post_title' => $title,
			'post_status' => 'draft',
			'post_type' => 'expression',
			]);
		
		if(is_wp_error($postId)) {
			$errors['title'] = "L'enregistrement n'a pas fonctionné, veuillez réessayer plus tard.";
		} else {
			update_post_meta($postId, 'name', $formname);
			update_post_meta($postId, 'description', $description);
			update_post_meta($postId, 'email', $email);

			$success = true;
		}
	}
}

global $post;
get_header('compiled');
get_view('hero');
?>
<div class="expression">
	<div class="page-inner">
		

		<? if($success): ?>
			<div class="form-message is-success">
				Votre message a bien été reçu, <br>
				Vous serez publié dans les plus brefs délais après validation par nos équipes.
			</div>
		<? else: ?>
		
			<div class="expression-presentation"><?= get_field('presentation') ?></div>
			<form class="form" method="post">

				<? if($errors): ?>
				<div class="form-message is-error">Veuillez corriger les erreurs ci-dessous</div>
				<? endif; ?>

				<div class="form-field <?= $errors['formname']?'form-error':'' ?>">
					<div class="form-error-msg"><?= $errors['formname'] ?></div>
					<input class="form-input" name="formname" type="text" placeholder="Nom à afficher" value="<?= $formname ?>">
				</div>
				<div class="form-field <?= $errors['email']?'form-error':'' ?>">
					<div class="form-error-msg"><?= $errors['email'] ?></div>
					<input class="form-input" name="email" type="text" placeholder="Adresse courriel" value="<?= $email ?>">
				</div>
				<div class="form-field <?= $errors['title']?'form-error':'' ?>">
					<div class="form-error-msg"><?= $errors['title'] ?></div>
					<input class="form-input" name="title" type="text" placeholder="Sujet" value="<?= $title ?>">
				</div>
				<div class="form-field <?= $errors['description']?'form-error':'' ?>">
					<div class="form-error-msg"><?= $errors['description'] ?></div>
					<textarea class="form-textarea" name="description" placeholder="Exprimez-vous..."><?= $description ?></textarea>
				</div>
				<button class="form-button">Envoyer</button>

			</form>
		<? endif; ?>

		<?
		$comments = [];
		$expressions = get_posts([
			'post_type' => 'expression',
			'posts_per_page' => -1
			]);
		if($expressions) :
			?>
			<h2 class="expression-title">Ils se sont exprimés...</h2>
			<div class="expression-list">
				<?
				foreach($expressions as $item):
					$hasLiked = $userLikes[$item->ID] != null;
					$countLikes = $wpdb->get_col($wpdb->prepare('SELECT count(*) FROM eopro_expression_like WHERE item_id = %d', $item->ID));
					$comments = get_comments(['post_id' => $item->ID, 'status' => 'approve']);
					?>
					<div class="expression-item">
						<h3 class="expression-item-title"><?= $item->post_title ?></h3>
						<div class="expression-item-description"><?= get_field('description', $item->ID) ?></div>
						<div class="expression-item-author">écrit par : <?= get_field('name', $item->ID) ? get_field('name', $item->ID) : 'Anonyme' ?></div>
						<div class="expression-item-reactions">
							<a class="expression-item-like <?= $hasLiked ? 'has-liked':'' ?>" data-like="<?= $item->ID ?>">
								<?= icon('voteup') ?>
								<span class="expression-item-reactions-count"><?= $countLikes[0] ?></span>
							</a>
							<a class="expression-item-comment" data-lightbox="commentForm" data-itemId="<?= $item->ID ?>">
								<?= icon('comment') ?>
								<span class="expression-item-reactions-count"><?= count($comments) ?></span>
							</a>
						</div>
						<?
						if($comments):
							?>
							<div class="expression-item-comments">
								<? 
								foreach($comments as $comment):
									?>
									<div class="expression-item-comments-item">
										<strong><?= $comment->comment_author ?> - </strong><?= $comment->comment_content ?>
									</div>
									<?
								endforeach;

								if(count($comments) >= 3):
									?>
									<div class="expression-item-comments-more">Afficher tous les commentaires...</div>
									<?
								endif;
								?>
							</div>
							<?
						endif;
						?>
					</div>
					<?
				endforeach;
				?>
			</div>
			<? 
		endif; 
		?>
	</div>

	<? get_view('commentForm') ?>
	
</div>

<? get_footer('compiled'); ?>