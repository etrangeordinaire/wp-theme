<?

/**
 * Template Name: Livrables
 */

global $post;
get_header('compiled');
get_view('hero');
?>
<div class="page-inner">
	<?
	foreach(get_field('phases') as $phase):
		?>
		<div class="livrables">
			<h2 class="livrables-phase"><?= $phase['title'] ?></h2>
			<div class="livrables-list">
				<?
				if($phase['livrables']):
				foreach($phase['livrables'] as $livrable):
					?>
					<a class="livrables-item" href="<?= $livrable['file']['url'] ?>" target="_blank">
						<? icon('download') ?>
						<div class="livrables-item-main">
							<h3 class="livrables-item-title"><?= $livrable['file']['filename'] ?></h3>
							<div class="livrables-item-subtitle"><?= $livrable['file']['title'] ?></div>
						</div>
					</a>
					<?
				endforeach;
				else:
					?>
					<span class="livrables-empty">Aucun livrable dans cette phase pour le moment...</span>
					<?
				endif;
				?>
			</div>
		</div>
		<?
	endforeach;
	?>
</div>
<? get_footer('compiled'); ?>