<?

/**
 * Template Name: Veille
 */

global $post;
get_header('compiled');
get_view('hero');
?>
<div class="page-inner">
	<?
	$dossiers = get_terms('dossier', [ 'hide_empty' => true ]);
	foreach($dossiers as $key => $dossier):
		$veilles = get_posts([
			'post_type' => 'veille',
			'tax_query' => [[
				'taxonomy' => 'dossier',
				'terms' => $dossier->term_id
			]],
			'posts_per_page' => -1
			]);
		?>
		<div class="dossier" id="dossier-<?= $key ?>">
			<div class="dossier-header">
				<h2 class="dossier-title"><?= $dossier->name ?></h2>
				
				<? if(current_user_can('edit_posts')): ?>
				<a class="dossier-add" 
				href="<?= site_url() ?>/wp-admin/post-new.php?post_type=veille&dossier=<?= $dossier->term_id ?>" 
				target="_blank"><? icon('plus') ?> Ajouter un article</a>
				<? endif; ?>
			</div>

			<div class="previews">
				<?
				foreach($veilles as $veille):
					get_view('veillePreview');
				endforeach;
				?>

				<? if(count($veilles) >= 3): ?>
				<a class="dossier-toggle" href="#" data-toggleClass="is-active" data-targetId="dossier-<?= $key ?>">
					<span class="dossier-toggle-more">Afficher les <?= count($veilles) ?> articles &darr;</span>
					<span class="dossier-toggle-less">Réduire &uarr;</span>
				</a>
				<? endif; ?>
			</div>

		</div>
		<?
	endforeach;
	?>
</div>
<? get_footer('compiled'); ?>