<?
global $wpdb;
session_start();
$itemId = get_query_var('item_id');
$vote = get_query_var('vote');
$sessionId = session_id();
$userIp = $_SERVER['REMOTE_ADDR'];
if(!$itemId || !$vote || !$sessionId || !$userIp) die('unauthorized call');

$terms = wp_get_post_terms($itemId, 'sondage');
if(!$terms) die('no sondage related');

$sondageId = $terms[0]->term_id;
if(!get_field('actif', 'sondage_'.$sondageId)) die('sondage inactif');

$voteBool = $vote == 'up' ? 1 : 0;

// check if vote exists
$getQuery = $wpdb->prepare('SELECT * FROM eopro_vote WHERE item_id = %d AND session_id LIKE %s', $itemId, $sessionId);
$row = $wpdb->get_row($getQuery);

// no vote => insert row
if(is_null($row)) {
	$wpdb->insert('eopro_vote', 
		[
			'item_id' => $itemId,
			'vote' => $voteBool,
			'IP' => $userIp,
			'session_id' => $sessionId,
			'sondage_id' => $sondageId
		], 
		['%d','%d','%s','%s']
		);
} 

// no same vote value => update row
elseif($row->vote != $voteBool) {
	$wpdb->update('eopro_vote', 
		[
			'vote' => $voteBool
		], 
		[
			'item_id' => $itemId,
			'session_id' => $sessionId
		],
		['%d'], ['%d', '%s']);
}
?>