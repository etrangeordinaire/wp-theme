<?
global $wpdb;
session_start();
$itemId = get_query_var('item_id');
$sessionId = session_id();
$userIp = $_SERVER['REMOTE_ADDR'];
if(!$itemId || !$sessionId || !$userIp) die('unauthorized call');

$item = get_post($itemId);
if(!$item) die('no item related');

// check if vote exists
$getQuery = $wpdb->prepare('SELECT * FROM eopro_expression_like WHERE item_id = %d AND session_id LIKE %s', $itemId, $sessionId);
$row = $wpdb->get_row($getQuery);

// no like => insert row
if(is_null($row)) {
	$wpdb->insert('eopro_expression_like', 
		[
			'item_id' => $itemId,
			'IP' => $userIp,
			'session_id' => $sessionId,
		], 
		['%d','%s','%s']
		);
} 
// like exists => delete row
else {
	$wpdb->delete('eopro_expression_like', 
		[
			'item_id' => $itemId,
			'session_id' => $sessionId
		],
		['%d', '%s']);
}
?>