<?	
use Abraham\TwitterOAuth\TwitterOAuth;
$access_token = '372950491-pBYNrsgRKgvvLZ9fuvOmgBy1tgP3KLxcqxyN7yOQ';
$access_token_secret = 'ho4pw01iRTHya2IqZZtLVsKn1KNFjYWYEOYruS2oUGCPC';
$connection = new TwitterOAuth(sb9UGsO9pC9nVxQZ4lZWeIk2n, dxruKWbho0PedTAkkFvOq5oIkqD7DAiMRftvHO5eNGR97fPhsp, $access_token, $access_token_secret);
$content = $connection->get("search/tweets", [
	'q' => $_GET['q'],
	'count' => 10,
	'max_id' => $_GET['max_id'] ? $_GET['max_id'] : null
	]);

// var_debug($content);
if($content->statuses):
foreach($content->statuses as $tweet):
	$datetime = new DateTime($tweet->created_at);
	$timestamp = $datetime->getTimestamp();

	// rm hashtags from content
	// $text = preg_replace('/#([\w-]+)/i', '', $tweet->text);
	$text = $tweet->text;
	
	// add <a> on URLs
	$reg_exUrl = "/(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
	if(preg_match($reg_exUrl, $text, $url)) {
       $text = preg_replace($reg_exUrl, '<a target="_blank" href="'.$url[0].'">'.$url[0].'</a>', $text);
	}

	// add <a> on @mention
	$reg_exUrl = "/@[a-zA-Z0-9]*/";
	if(preg_match($reg_exUrl, $text, $mention)) {
       $text = preg_replace($reg_exUrl, '<a target="_blank" href="https://twitter.com/'.$mention[0].'">'.$mention[0].'</a>', $text);
	}

	?>
	<div class="tweet" data-tweetId="<?= $tweet->id ?>">
		<div class="tweet-date">
			Le <?= date_i18n('j F Y', $timestamp); ?><br>
			à <?= date_i18n('H:i', $timestamp); ?>
		</div>
		<div class="tweet-main">
			<a href="https://twitter.com/<?= $tweet->user->screen_name ?>" class="tweet-author" target="_blank">
				<img class="tweet-author-visual" src="<?= $tweet->user->profile_image_url ?>">
				@<?= $tweet->user->screen_name ?>
			</a>
			<div class="tweet-content"><?= $text ?></div>
			<ul class="tweet-hashtags">
				<?
				foreach($tweet->entities->hashtags as $hashtag):
					?>
					<li class="tweet-hashtag">
						<a href="https://twitter.com/search?q=<?= $hashtag->text ?>" target="_blank">
							#<?= $hashtag->text ?>
						</a>
					</li>
					<?
				endforeach;
				?>
			</ul>
		</div>
	</div>
	<?
endforeach;
endif;
?>