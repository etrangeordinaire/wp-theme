<?

// get item
$itemId = get_query_var('item_id');
if(!$itemId) die('unauthorized call');
$item = get_post($itemId);
if(!$item) die('no item related');

// vars
$name = $_REQUEST['name'];
$email = $_REQUEST['email'];
$comment = $_REQUEST['comment'];

$errors = [];
if(!$name) $errors['name'] = "Ce champs est obligatoire";
if(!$email) $errors['email'] = "Ce champs est obligatoire";
elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) $errors['email'] = "Cette adresse n'est pas valide";
if(!$comment) $errors['comment'] = "Ce champs est obligatoire";

if(!empty($errors)) returnResponse($errors, 400);

wp_insert_comment([
	'comment_post_ID' => $itemId,
	'comment_author' => $name,
	'comment_content' => $comment,
	'comment_author_email' => $email,
	'comment_approved' => 0
	]);

http_response_code(200);
?>