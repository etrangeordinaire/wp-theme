<?

// retrieve user votes
global $wpdb;
session_start();
$query = $wpdb->prepare('SELECT * FROM eopro_vote WHERE session_id LIKE %s', session_id());
$results = $wpdb->get_results($query);
$votes = [];
foreach($results as $vote) $votes[$vote->item_id] = $vote;

$sondage = get_term(get_queried_object()->term_id, 'sondage');

// retrieve sondage stats
$countVote = $wpdb->get_col($wpdb->prepare('SELECT count(*) FROM eopro_vote WHERE sondage_id = %d', $sondage->term_id));
$countUser = $wpdb->get_col($wpdb->prepare('SELECT count(distinct(session_id)) FROM eopro_vote WHERE sondage_id = %d', $sondage->term_id));

get_header('compiled');
?>
<div class="sondage <?= !get_field('actif', 'sondage_'.$sondage->term_id)?'sondage-past':'' ?>">


	<div class="sondage-header">
		<div class="sondage-infos">
			<h1 class="sondage-title"><?= $sondage->name ?></h1>
			<h2 class="sondage-subtitle"><?= get_field('subtitle', 'sondage_'.$sondage->term_id) ?></h2>
			<div class="sondage-description"><?= $sondage->description ?></div>
		</div>
		<div class="sondage-metas">
			<div class="sondage-meta">
				<h3 class="sondage-meta-title">Statut :</h3>
				<div class="sondage-meta-content"><?= get_field('actif', 'sondage_'.$sondage->term_id)=='1'?'En cours':'Passé' ?></div>
			</div>
			<div class="sondage-meta">
				<h3 class="sondage-meta-title">Période :</h3>
				<div class="sondage-meta-content"><?= get_field('date', 'sondage_'.$sondage->term_id) ?></div>
			</div>
			<div class="sondage-meta">
				<h3 class="sondage-meta-title">Activités :</h3>
				<div class="sondage-meta-content">
					<ul>
						<li><strong>Nb de participants :</strong> <?= $countUser[0] ?></li>
						<li><strong>Nb de vote :</strong> <?= $countVote[0] ?></li>
					</ul>
				</div>
			</div>
		</div>
	</div>


	<?
	$axes = get_terms([ 'taxonomy' => 'axe' ]);
	foreach($axes as $axe):
		$propositions = get_posts([
				'post_type' => 'proposition',
				'posts_per_page' => -1,
				'orderby' => 'title',
				'order' => 'ASC',
				'tax_query' => [
					[ 'taxonomy' => 'axe', 'terms' => $axe->term_id ],
					[ 'taxonomy' => 'sondage', 'terms' => $sondage->term_id ]
				]
				]);
		if(!empty($propositions)):
		?>
		<div class="sondage-axe">
			<h2 class="sondage-axe-title"><?= $axe->name ?></h2>
			<div class="sondage-list">
			<?
			foreach($propositions as $proposition):
				$userVote = $votes[$proposition->ID] ? $votes[$proposition->ID]->vote : null ;
				$countUp = $wpdb->get_col($wpdb->prepare('SELECT count(*) FROM eopro_vote WHERE item_id = %d AND vote = 1', $proposition->ID));
				$countDown = $wpdb->get_col($wpdb->prepare('SELECT count(*) FROM eopro_vote WHERE item_id = %d AND vote = 0', $proposition->ID));
				$comments = get_comments(['post_id' => $proposition->ID, 'status' => 'approve']);
				$visual = wp_get_attachment_image_src( get_post_thumbnail_id($proposition->ID), 'horizontal' )[0];
				?>
				<div class="proposition">
					<div class="proposition-header">
						<div class="proposition-header-inner">
							<? if($visual): ?>
							<img class="proposition-visual" src="<?= $visual ?>">
							<? endif; ?>
							<h3 class="proposition-title">
								<?= $proposition->post_title ?>
								<span class="proposition-more">&#x25BC;</span>
							</h3>
							<div class="proposition-ctas">
								<a class="proposition-cta <?= '1'===$userVote?'has-voted':'' ?>" data-vote="up" data-voteId="<?= $proposition->ID ?>" href="#">
									<? icon('voteup') ?>
									<span class="proposition-cta-count"><?= $countUp[0] ?></span>
								</a>
								<a class="proposition-cta <?= '0'===$userVote?'has-voted':'' ?>" data-vote="down" data-voteId="<?= $proposition->ID ?>" href="#">
									<? icon('votedown') ?>
									<span class="proposition-cta-count"><?= $countDown[0] ?></span>
								</a>
							</div>
						</div>
					</div>
					<div class="proposition-body">
						<div class="proposition-body-inner">

							<div class="proposition-content"><?= apply_filters('the_content', $proposition->post_content) ?></div>
							

							<div class="proposition-comments">
								<?
								if(!empty($comments)):
									?>
									<div class="proposition-comments-list">
										<? 
										foreach($comments as $comment):
											?>
											<div class="proposition-comments-item">
												<strong><?= $comment->comment_author ?> - </strong><?= $comment->comment_content ?>
											</div>
											<?
										endforeach;
										?>
									</div>
									<?
								endif;
								?>
								<a class="proposition-comments-cta" data-lightbox="commentForm" data-itemId="<?= $proposition->ID ?>"><? icon('comment') ?> Écrire un commentaire...</a>
							</div>
						</div>
					</div>
				</div>
				<?
			endforeach;
			?>
			</div>
		</div>
		<?
		endif;
	endforeach;
	?>


	<? get_view('commentForm') ?>


</div>
<? get_footer('compiled'); ?>